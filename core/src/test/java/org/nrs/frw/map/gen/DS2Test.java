package org.nrs.frw.map.gen;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;

public class DS2Test {

	private final float [][] TEST_INPUT_1 = {
			{1, Float.NaN, Float.NaN, Float.NaN, 0.5f},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{0.75f, Float.NaN, Float.NaN, Float.NaN, 1.0f},
	};
	
	private final float [][] TEST_INPUT_2 = {
			{1, Float.NaN, Float.NaN, Float.NaN, 0.5f},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{Float.NaN, Float.NaN, 0.3f, Float.NaN, Float.NaN},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{0.75f, Float.NaN, Float.NaN, Float.NaN, 1.0f},
	};

	private final float [][] TEST_INPUT_3 = {
			{1, Float.NaN, 0.01f, Float.NaN, 0.5f},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{0.01f, Float.NaN, 0.3f, Float.NaN, 0.01f},
			{Float.NaN, Float.NaN, Float.NaN, Float.NaN, Float.NaN},
			{0.75f, Float.NaN, 0.01f, Float.NaN, 1.0f},
	};

	private DS2 ds2;
	
	@Before
	public void createDS() {
		int pot = 2;
		ds2 = new DS2(pot);
	}

	@Test 
	public void shouldHaveRandomSeed() {
		int pot = 2; //power of two (pot)
		DS2 ds2_1 = new DS2(pot);
		DS2 ds2_2 = new DS2(pot);

		assertNotNull(ds2_1.getSeed());
		assertNotNull(ds2_2.getSeed());
		assertNotEquals(ds2_1.getSeed(), ds2_2.getSeed());
	}
	
	@Test 
	public void shouldHavePresetSeed() {
		int pot = 2; //power of two (pot)
		DS2 ds2_1 = new DS2(pot, 1);
		DS2 ds2_2 = new DS2(pot, 1);

		assertEquals(1, ds2_1.getSeed());
		assertEquals(1, ds2_2.getSeed());
		assertEquals(ds2_1.getSeed(), ds2_2.getSeed());
		
		ds2_1.generate();
		ds2_2.generate();

		float [][] a1 = ds2_1.getData();
		float [][] a2 = ds2_2.getData();
		
		for( int y=0; y<a1.length; y++ ) {
			for( int x=0; x<a1[y].length; x++ ) {
				assertEquals(a1[y][x], a2[y][x], 0.001);
			}
		}
	}
	
	@Test
	public void shouldGenerate5x5Array() {
		
		ds2.generate();
		float [][] array = ds2.getData();
		
		printDataArray("shouldGenerate5x5Array", array);
		
		assertEquals(5, array.length);
		assertEquals(5, array[0].length);
	}
	
	@Test
	public void shouldGenerateSeeded5x5Array() {
				
		ds2.generate();
		float [][] array = ds2.getData();
		
		printDataArray("shouldGenerateSeeded5x5Array", array);
		
		assertFalse(Float.isNaN(array[0][0]));
		assertFalse(Float.isNaN(array[4][0]));
		assertFalse(Float.isNaN(array[4][4]));
		assertFalse(Float.isNaN(array[0][4]));
	}
	
	@Test
	public void shouldGeneratePopulated5x5Array() {
		
		ds2.generate();
		float [][] array = ds2.getData();
		
		printDataArray("shouldGeneratePopulated5x5Array", array);
		
		for( float [] r : array ) {
			for( float val : r) {
				assertFalse(Float.isNaN(val));
			}
		}
	}

	@Test
	public void shouldGeneratePopulated5x5ArrayWrapped() {
		
		ds2.setVariance(1.0f);
		
		ds2.setWrapped(true);
		ds2.generate();
		float [][] array = ds2.getData();
		
		printDataArray("shouldGeneratePopulated5x5ArrayWrapped", array);
		
		for( int x = 0; x<array[0].length; x++ ) {
			assertEquals(array[0][x], array[array.length-1][x], 0.001);
		}

		for( int y = 0; y<array.length; y++ ) {
			assertEquals(array[y][0], array[y][array[0].length-1], 0.001);
		}
	}

	@Test
	public void shouldGenerateInitialNaNValues() {
		float [][] array = ds2.getData();
		
		for( float [] row : array ) {
			 for( float val : row ) {
				 assertTrue(Float.isNaN(val));
			 }
		}
	}
	
	@Test
	public void shouldSeedCorners() {
		ds2.seed();
		
		float [][] array = ds2.getData();
		
		assertFalse(Float.isNaN(array[0][0]));
		assertFalse(Float.isNaN(array[0][4]));
		assertFalse(Float.isNaN(array[4][4]));
		assertFalse(Float.isNaN(array[4][0]));
	}

	@Test
	public void givenSeedShouldSeedCornersWithSameRandomValues() {
		int pot = 2; //power of two (pot)
		DS2 ds2 = new DS2(pot, 1);

		ds2.seed();
		
		float [][] array = ds2.getData();
		printDataArray("givenSeeShouldSeedCornersWithSameRandomValues", array);
		
		assertEquals(0.7308782f, array[0][0], 0.01);
		assertEquals(0.100473166f, array[0][4], 0.01);
		assertEquals(0.4100808f, array[4][4], 0.01);
		assertEquals(0.40743977f, array[4][0], 0.01);
	}
	
	@Test
	public void givenSeededArrayShouldPopulateCorners() {
		ds2.setData(TEST_INPUT_1);
		
		float [][] array = ds2.getData();
		
		assertEquals(TEST_INPUT_1[0][0], array[0][0], 0.01);
		assertEquals(TEST_INPUT_1[0][4], array[0][4], 0.01);
		assertEquals(TEST_INPUT_1[4][4], array[4][4], 0.01);
		assertEquals(TEST_INPUT_1[4][0], array[4][0], 0.01);
	}

	@Test
	public void givenSeededArraySquareShouldComputeCenterAverage() {
		ds2.setData(TEST_INPUT_1);
		
		ds2.square();
		float [][] array = ds2.getData();
		printDataArray("givenSeededArraySquareShouldComputeCenterAverage", array);
		
		float expected = (TEST_INPUT_1[0][0] + TEST_INPUT_1[0][4] + TEST_INPUT_1[4][4] + TEST_INPUT_1[4][0])/4;
		assertEquals(expected, array[2][2], 0.001 );
	}
	
	@Test
	public void givenCenterSeededArraySquareShouldNotOverrideValue() {
		ds2.setData(TEST_INPUT_2);
		
		ds2.square();
		
		float [][] array = ds2.getData();
		printDataArray("givenCenterSeededArraySquareShouldNotOverrideValue", array);
		
		float expected = 0.3f;
		assertEquals(expected, array[2][2], 0.001 );
	}

	@Test
	public void givenSeededArrayDiamondShouldPopulateMidValues() {
		ds2.setData(TEST_INPUT_2);

		ds2.diamond();
		
		float [][] array = ds2.getData();
		printDataArray("givenSeededArrayDiamondShouldPopulateMidValues", array);

		assertFalse(Float.isNaN(array[0][2]));
		assertFalse(Float.isNaN(array[2][0]));
		assertFalse(Float.isNaN(array[4][2]));
		assertFalse(Float.isNaN(array[2][4]));
		
		//top center
		float expectedMidVal = (TEST_INPUT_2[2][2] + TEST_INPUT_2[0][0] + TEST_INPUT_2[0][4]) / 3;
		assertEquals(expectedMidVal, array[0][2], 0.001);

		//left center
		expectedMidVal = (TEST_INPUT_2[2][2] + TEST_INPUT_2[0][0] + TEST_INPUT_2[4][0]) / 3;
		assertEquals(expectedMidVal, array[2][0], 0.001);

		//right center
		expectedMidVal = (TEST_INPUT_2[2][2] + TEST_INPUT_2[0][4] + TEST_INPUT_2[4][4]) / 3;
		assertEquals(expectedMidVal, array[2][4], 0.001);

		//bottom center
		expectedMidVal = (TEST_INPUT_2[2][2] + TEST_INPUT_2[4][0] + TEST_INPUT_2[4][4]) / 3;
		assertEquals(expectedMidVal, array[4][2], 0.001);
	}
	
	@Test
	public void givenSeededArrayAndXAndYStartValsSquareShouldPopulateCenterValuesForSubArray() {
		ds2.setData(TEST_INPUT_3);

		ds2.square(3, 3, 1);
		
		float [][] array = ds2.getData();
		printDataArray("givenSeededArrayAndXAndYStartValsSquareShouldPopulateCenterValuesForSubArray", array);

		assertFalse(Float.isNaN(array[3][3]));
	}

	@Test
	public void givenMidSeededArrayDiamondShouldNotOverrideValue() {
		ds2.setData(TEST_INPUT_3);
		
		ds2.diamond();
		
		float [][] array = ds2.getData();
		printDataArray("givenMidSeededArrayDiamondShouldNotOverrideValue", array);
		
		float expected = 0.01f;
		assertEquals(expected, array[0][2], 0.001);
		assertEquals(expected, array[2][0], 0.001);
		assertEquals(expected, array[2][4], 0.001);
		assertEquals(expected, array[4][2], 0.001);
	}
	
	@Test
	public void shouldReturnMax() {
		ds2.generate();
		
		float [][] data = ds2.getData();
		float max = ds2.getMax();
		boolean maxFound = false;

		for( float[] row : data) {
			for(float val : row) {
				assertTrue(val <= max);
				if( val == max ) {
					maxFound = true;
				}
			}
		}
		
		assertTrue(maxFound);
	}

	@Test
	public void shouldReturnMin() {
		
		ds2.generate();
		
		float [][] data = ds2.getData();
		float min = ds2.getMin();
		boolean minFound = false;

		for( float[] row : data) {
			for(float val : row) {
				assertTrue(val >= min);
				if( val == min ) {
					minFound = true;
				}
			}
		}
		
		assertTrue(minFound);
	}
	
	@Test
	public void shouldMaintainSeedValuesforWrappedMap00() {
		ds2.setWrapped(true);

		float [][] data = ds2.getData();
		data[0][0] = -1.0f;
		
		ds2.generate();
		
		data = ds2.getData();
		
		assertEquals(-1.0f, data[0][0], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[ds2.getSize()-1][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
	}

	@Test
	public void shouldMaintainSeedValuesforWrappedMap0Size() {
		ds2.setWrapped(true);

		float [][] data = ds2.getData();
		data[0][ds2.getSize()-1] = -1.0f;
		
		ds2.generate();
		
		data = ds2.getData();
		
		assertEquals(-1.0f, data[0][0], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[ds2.getSize()-1][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
	}

	@Test
	public void shouldMaintainSeedValuesforWrappedMapSizeSize() {
		ds2.setWrapped(true);

		float [][] data = ds2.getData();
		data[ds2.getSize()-1][ds2.getSize()-1] = -1.0f;
		
		ds2.generate();
		
		data = ds2.getData();
		
		assertEquals(-1.0f, data[0][0], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[ds2.getSize()-1][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
	}

	@Test
	public void shouldMaintainSeedValuesforWrappedMapSize0() {
		ds2.setWrapped(true);

		float [][] data = ds2.getData();
		data[ds2.getSize()-1][0] = -1.0f;
		
		ds2.generate();
		
		data = ds2.getData();
		
		assertEquals(-1.0f, data[0][0], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[ds2.getSize()-1][ds2.getSize()-1], 0.001);
		assertEquals(-1.0f, data[0][ds2.getSize()-1], 0.001);
	}

	//@Test
	public void writeImage() {
		
		DS2 d2 = new DS2(10);
		d2.setVariance(1f);
		d2.setWrapped(true);
		float [][] data = d2.getData();
		data[512][512] = 1.0f;
		data[0][0] = -1.0f;
		data[510][510] = -2.0f;
		data[509][510] = -2.0f;
		data[509][509] = -2.0f;
		data[509][508] = -2.0f;
		d2.generate();
		
		//data = d2.getData();
		String fileName = "test.png";
		
		BufferedImage bi = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		
		System.out.println("Min, Max [" + d2.getMin() + ", " + d2.getMax() + "]");
		for( int x=0; x<data.length; x++ ) {
			for( int y=0; y<data[x].length; y++ ) {
				float col = (data[y][x]-d2.getMin())/(d2.getMax()-d2.getMin());
				
				try {
					g.setColor(new Color(col, col, col));
				} catch( Exception ex) {
					System.out.println("Data: " + data[y][x]);
					System.out.println("C: " + col + " [" + x + ", " + y + "]");
					System.exit(1);
				}
				
				g.drawRect(x, y, 1, 1);
			}
		}
		
		File f = new File(fileName);
		try {
			ImageIO.write(bi, "PNG", f);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}

	public void printDataArray(String label, float [][] array) {
		
		System.out.print("\n" + label + ":\n{");
		for( int y = 0; y<array.length; y++ ) {
			System.out.print("{");
			for( int x = 0; x<array[y].length; x++ ) {
				System.out.print(array[y][x] + "f" + (x < array[y].length-1 ? ", " : ""));
			}
			System.out.println("}" + (y < array.length-1 ? "," : "}"));
		}		
	}
}
