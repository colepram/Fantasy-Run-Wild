package org.nrs.frw.map.gen;

import static org.junit.Assert.*;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.imageio.ImageIO;

import org.junit.Before;
import org.junit.Test;
import org.nrs.frw.map.gen.voronoi.FCircle;
import org.nrs.frw.map.gen.voronoi.FEdge;
import org.nrs.frw.map.gen.voronoi.FPoint;
import org.nrs.frw.map.gen.voronoi.FSite;
import org.nrs.frw.map.gen.voronoi.Fortune;
import org.nrs.frw.map.gen.voronoi.Fortune.SiteEventListener;

public class MapGenTest {

	private MapGen mg;
	
	@Before
	public void createMapGen() {
		mg = new MapGen(6);
	}
	
	@Test
	public void init() {
		assertNotNull(mg);
	}
	
	@Test
	public void shouldGet65x65Grid() {
		float [][] grid = mg.getGrid();
		
		assertEquals(65, grid.length);
		assertEquals(65, grid[0].length);
	}
	
	@Test
	public void shouldPopulateGridWithNaN() {
		float [][] grid = mg.getGrid();
		
		for( int y=0; y<mg.getSize(); y++ ) {
			for( int x=0; x<mg.getSize(); x++ ) {
				assertEquals(Float.NaN, grid[y][x], 0.1);
			}
		}
	}

	@Test
	public void shouldReturnGridSize() {
		assertEquals(65, mg.getSize());
	}
	
	@Test
	public void shouldReturnSubPoTSize() {
		assertEquals(3, mg.getSubSize());
	}
	
	@Test
	public void shouldRandomlySpacedPointsOntoGrid() {
		
		int size = 17;
		float input[][] = new float[size][size];
		for( int y=0; y<input.length; y++ ) {
			for(int x=0; x<input[y].length; x++ ) {
				int val = y*10 + x;
				input[y][x] = (float)val;
			}
		}
		
		mg.spacePoints(input);
		
		printDataArray("shouldRandomlySpacedPointsOntoGrid", mg.getGrid());
		
		int pointCount=0;
		for( float [] r : mg.getGrid() ) {
			for( float val : r ) {
				if( !Float.isNaN(val) ) {
					pointCount++;
				}
			}
		}
		
		assertEquals(size*size, pointCount);
	}
	
	@Test
	public void testInputToFortune() {
		
		File out = new File("out");
		if( out.exists() ) {
			for( File f : out.listFiles() ) {
				f.delete();
			}
			out.delete();
		}

		out.mkdirs();
		
		MapGen mg = new MapGen(4);
		int size = 3;
		float input[][] = new float[size][size];
		for( int y=0; y<input.length; y++ ) {
			for(int x=0; x<input[y].length; x++ ) {
				int val = y*10 + x;
				input[y][x] = (float)val;
			}
		}
		
		mg.spacePoints(input);
		
		SiteEventListener sel = new Fortune.SiteEventListener() {

			private int processed = 0;
			
			private BufferedImage drawSites() {
				BufferedImage bi = new BufferedImage(mg.getGrid().length, mg.getGrid()[0].length, BufferedImage.TYPE_INT_RGB);
				Graphics2D g = bi.createGraphics();
				
				for( FCircle c : getCircles() ) {
					if( !c.isDeleted() ) {
						Ellipse2D circle = new Ellipse2D.Double();
						circle.setFrame(c.getCenter().getX()-c.getRadius(), c.getCenter().getY()-c.getRadius(), c.getRadius()*2, c.getRadius()*2);
						g.setColor(Color.GRAY );
						g.draw(circle);
					}
				}
				
				for( FEdge e : getEdges() ) {
					g.setColor(Color.RED);
					if( e.getEnd() != null ) {
						g.drawLine((int)e.getStart().getX(), (int)e.getStart().getY(), (int)e.getEnd().getX(), (int)e.getEnd().getY());
					}

					g.setColor(Color.YELLOW);
					g.drawLine((int)e.getStart().getX(), (int)e.getStart().getY(), (int)e.getStart().getX(), (int)e.getStart().getY());
					
					g.setColor(Color.GREEN);
					if( e.getEnd() != null ) {
						g.drawLine((int)e.getEnd().getX(), (int)e.getEnd().getY(), (int)e.getEnd().getX(), (int)e.getEnd().getY());
					}
				}
				
				for( FCircle c : getCircles() ) {
					if( !c.isDeleted() ) {
						g.setColor( Color.GREEN );
						g.drawLine((int)c.getX(), (int)c.getY(), (int)c.getX(), (int)c.getY());
						g.setColor( Color.CYAN );
						g.drawLine((int)c.getCenter().getX(), (int)c.getCenter().getY(), (int)c.getCenter().getX(), (int)c.getCenter().getY());
					}
				}
				
				g.setColor(Color.BLUE);
				for( FSite e : getSites() ) {
					g.drawLine((int)e.getX(), (int)e.getY(), (int)e.getX(), (int)e.getY());
				}
				
				return bi;
			}
			
			@Override
			public void siteProcessed() {
				BufferedImage bi = drawSites();
				
				File outf = new File("out/" +processed + "_test.png");
				try {
					ImageIO.write(bi, "PNG", outf);
				} catch (IOException e) {
					e.printStackTrace();
				}
				processed++;
			}

		};
		
		Fortune f = new Fortune(sel);
		int found = f.loadQueue(mg.getGrid());
		f.processPoints();

		System.out.print("Sites Found: ");
		System.out.println(found);
		System.out.print("Sites Processed: ");
		System.out.println(Fortune.sitesProcessed);
		System.out.print("Circles Processed: ");
		System.out.println(Fortune.circlesProcessed);
		System.out.print("Points Created: ");
		System.out.println(FPoint.pointsCreated);
		System.out.print("Sites Created: ");
		System.out.println(FSite.sitesCreated);
		System.out.print("Circles Created: ");
		System.out.println(FCircle.circlesCreated);
		System.out.print("Edges Created: ");
		System.out.println(FEdge.edgesCreated);

		Collection<FEdge> edges = f.getEdges();
		assertNotNull(edges);		
	}
	
	public void printDataArray(String label, float [][] array) {
		
		System.out.print("\n" + label + ":\n{");
		for( int y = 0; y<array.length; y++ ) {
			System.out.print("{");
			for( int x = 0; x<array[y].length; x++ ) {
				System.out.print(array[y][x] + "f" + (x < array[y].length-1 ? ", " : ""));
			}
			System.out.println("}" + (y < array.length-1 ? "," : "}"));
		}		
	}

}
