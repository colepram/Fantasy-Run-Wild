package org.nrs.frw.map.gen;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.nrs.frw.map.SessionManagerInterface;

import com.badlogic.gdx.graphics.Texture;

public class MapData2Test {

	private static final int EXPECTED_MAP_HEIGHT = 5;

	private static final int EXPECTED_MAP_WIDTH = 5;

	private static final float [][] EXPECTED_DATA = {{0.97282356f, 0.95085436f, 0.6321421f, 0.82245994f, 0.97282356f},
			{0.34102544f, 0.5123694f, 0.3105517f, 0.4115351f, 0.34102544f},
			{0.62903625f, 0.8302299f, 0.50295985f, 0.38012066f, 0.62903625f},
			{1.0f, 0.5858738f, 0.6564543f, 0.77765834f, 1.0f},
			{0.97282356f, 0.95085436f, 0.6321421f, 0.82245994f, 0.97282356f}};
			
	private MapData2 data;
	
	@Before
	public void init() {
		data = new MapData2(new MockSessionManager());
	}
	
	@Test
	public void testInit() {
		assertNotNull(data);
		
		float mdata [][] = data.getMapData();
		
		printDataArray("InitSub", mdata);

		assertNotNull(mdata);
		assertEquals(EXPECTED_MAP_HEIGHT, mdata.length);
		assertEquals(EXPECTED_MAP_WIDTH, mdata[0].length);

		System.out.print("{");
		for( int y = 0; y<mdata.length; y++ ) {
			System.out.print("{");
			for( int x = 0; x<mdata[y].length; x++ ) {
				System.out.print(mdata[y][x] + "f" + (x < mdata[y].length-1 ? ", " : ""));
				assertEquals(EXPECTED_DATA[y][x], mdata[y][x], 0.001);
			}
			System.out.println("}" + (y < mdata.length-1 ? "," : "}"));
		}
	}
	
	@Test
	public void shouldGenerate5x5Chunk() {
		float [][] chunk = data.getChunk(0, 0);

		printDataArray("GenerateSub", chunk);
		
		assertNotNull(chunk);

		assertEquals(data.getMapData()[0][0], chunk[0][0], 0.01);
		assertEquals(data.getMapData()[0][1], chunk[0][2], 0.01);
		assertEquals(data.getMapData()[0][2], chunk[0][4], 0.01);
		assertEquals(data.getMapData()[1][0], chunk[2][0], 0.01);
		assertEquals(data.getMapData()[1][1], chunk[2][2], 0.01);
		assertEquals(data.getMapData()[1][2], chunk[2][4], 0.01);
		assertEquals(data.getMapData()[2][0], chunk[4][0], 0.01);
		assertEquals(data.getMapData()[2][1], chunk[4][2], 0.01);
		assertEquals(data.getMapData()[2][2], chunk[4][4], 0.01);
	}

	@Test
	public void testGetYChunks() {
		int expectedChunks = (int) Math.pow(2, MockSessionManager.mapSize) + 1;
		assertEquals(expectedChunks/3, data.getYChunks());
	}
	
	@Test
	public void testGetXChunks() {
		int expectedChunks = (int) Math.pow(2, MockSessionManager.mapSize) + 1;
		assertEquals(expectedChunks/3, data.getXChunks());
	}
	
	public void printDataArray(String label, float [][] array) {
		
		System.out.print("\n" + label + ":\n{");
		for( int y = 0; y<array.length; y++ ) {
			System.out.print("{");
			for( int x = 0; x<array[y].length; x++ ) {
				System.out.print(array[y][x] + "f" + (x < array[y].length-1 ? ", " : ""));
			}
			System.out.println("}" + (y < array.length-1 ? "," : "}"));
		}		
	}
	
	class MockSessionManager implements SessionManagerInterface {

		public static final int mapSize = 6;
		public static final int mapSeed = 1;
		
		@Override
		public int getTileSize() {
			return 0;
		}

		@Override
		public int getMaxTileId() {
			return 0;
		}

		@Override
		public Texture getTile(int tileID) {
			return null;
		}

		@Override
		public int getBufferRange() {
			return 0;
		}

		@Override
		public int getMapSize() {
			return mapSize;
		}

		@Override
		public int getMapSeed() {
			return mapSeed;
		}
		
	}
}
