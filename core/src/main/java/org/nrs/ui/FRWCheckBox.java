package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;

public class FRWCheckBox extends CheckBox {

	public FRWCheckBox(String label) {
		super(label, GameConstants.uiSkin);
		super.setColor(GameConstants.getUIColor());
		getImage().setColor(GameConstants.getUIColor());
	}
}