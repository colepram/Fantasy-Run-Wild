package org.nrs.ui;

import org.nrs.frw.GameConstants;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;

public class FRWImageButton extends ImageButton {

	public FRWImageButton() {
		super(GameConstants.uiSkin);
		super.setColor(GameConstants.getUIColor());
	}
}
