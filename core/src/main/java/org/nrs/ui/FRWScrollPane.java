package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;

public class FRWScrollPane extends ScrollPane {

	public FRWScrollPane(Actor widget) {
		super(widget, GameConstants.uiSkin);
		super.setColor(GameConstants.getUIColor());
	}
}
