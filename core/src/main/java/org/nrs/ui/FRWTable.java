package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class FRWTable extends Table {

	public FRWTable(String style) {
		this(style, null);
	}

	public FRWTable(String style, Color color) {
		super(GameConstants.uiSkin);
		super.setBackground(style);
		
		if( color!=null ) {
			setColor(color);
		}
	}
	
	public FRWTable() {
		super(GameConstants.uiSkin);
	}
}
