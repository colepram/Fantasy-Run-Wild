package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.scenes.scene2d.ui.TextField;

public class FRWTextField extends TextField {

	public FRWTextField(String string) {
		super(string, GameConstants.uiSkin);
		super.setColor(GameConstants.getUIColor());
	}

}
