package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

public class FRWButton extends Button {

	public FRWButton() {
		super(GameConstants.uiSkin);
		super.setColor(GameConstants.getUIColor());
	}
}
