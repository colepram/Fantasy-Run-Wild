package org.nrs.ui;

import org.nrs.frw.GameConstants;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class FRWTextButton extends TextButton {

	public FRWTextButton(String text) {
		super(text, GameConstants.uiSkin);
		setColor(GameConstants.getUIColor());
		padTop(2);
		padBottom(2);
	}

}
