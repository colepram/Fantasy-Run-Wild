package org.nrs.res;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Random;

public class NameGenerator {

	public static final int MALE = 0;
	public static final int FEMALE = 1;
	
	private static final String MALE_FILE = "MaleNames.txt";
	private static final String FEMALE_FILE = "FemaleNames.txt";
	
	public static String getName(int type) {
		String file = type==MALE?MALE_FILE:FEMALE_FILE;
		String name = "";
		
		//make sure dictionary is in the assets/data/ directory
		InputStream is = NameGenerator.class.getResourceAsStream("/data/" + file);
		
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		Object [] obj = br.lines().toArray();
		
		Random r = new Random();
		int rand = r.nextInt(obj.length);
		
		name = (String)obj[rand];

		return name.substring(0, 1) + name.substring(1, name.length()).toLowerCase();
	}

	public static void main(String [] args) {
		for( int i=0; i<100; i++ ) {
			System.out.println(getName(MALE));
		}
	}
}
