package org.nrs.state;

import java.util.HashMap;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.frw.character.Player;
import org.nrs.ui.FRWTable;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class CharacterGenState2 extends AGameState {

	Player p;
	
	public CharacterGenState2(FantasyRunWild game) {
		super(game);
		
		p = new Player(game) {

			@Override
			public void initDefaults() {
				getColorMap().put("C0", Color.TAN);
				getColorMap().put("C1", Color.LIME);
				getColorMap().put("C2", Color.YELLOW);

				getLayers().add("F0_L601_S0_C1_default");//eyes/color
				getLayers().add("F0_L600_S0_default");//eyes
				getLayers().add("F0_L500_S2_C2_default");//hair
				getLayers().add("F0_L400_S0_C0_default");//head
				getLayers().add("F0_L300_S0_C0_default");//arms
				getLayers().add("F0_L250_S0_C0_default");//body
				getLayers().add("F0_L200_S0_C0_default");//legs
				getLayers().add("F0_L100_S0_C0_default");//feet
			}			
		};
		
		//p.setPosition(100, 100);
		p.setScale(1f, 1f);
//		stage.addActor(p);

		FRWTable tDesign = new FRWTable();
		tDesign.setFillParent(true);
		
		FRWTable tc1 = new FRWTable("default-btn", Color.GRAY);
		tc1.add(p).grow();

		tDesign.add(tc1);
		
		tDesign.pack();
		stage.addActor(tDesign);
	}

	@Override
	protected void poleDebug() {
		stage.console.setText("Character Gen 2");
	}
}
