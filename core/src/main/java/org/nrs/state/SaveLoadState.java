package org.nrs.state;

import java.io.IOException;
import java.util.UUID;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.frw.character.FRWCharacter;
import org.nrs.ui.FRWScrollPane;
import org.nrs.ui.FRWTable;
import org.nrs.ui.FRWTextButton;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class SaveLoadState extends AGameState {

	private SLListener listener = new SLListener();
	
	public SaveLoadState(FantasyRunWild game) {
		super(game);
		
		//ScrollableList<SpriteActor> sp = new ScrollableList<SpriteActor>(GameConstants.uiSkin);

		FRWTable tbl_chars = new FRWTable();
		FileHandle fh = Gdx.files.local(GameConstants.SAVE_DIR);
		
		FileHandle [] saves = fh.list();
		for( FileHandle save : saves ) {
			UUID uuid = UUID.fromString(save.path().replaceAll(GameConstants.SAVE_DIR, ""));
			tbl_chars.add(getListElement(uuid)).growX().left().row();
		}
		//tbl_chars.pack();
		
		ScrollPane sp = new FRWScrollPane(tbl_chars);
		sp.setFadeScrollBars(false);
		sp.invalidate();
		
		FRWTable tDesign = new FRWTable();
		tDesign.setFillParent(true);
		tDesign.add(sp).grow();
		tDesign.pad(10,10,10,10);
		tDesign.pack();
		
		stage.addActor(tDesign);
		
	}
	
	private Table getListElement(UUID uuid) {
		try {
			FRWCharacter cha = new FRWCharacter(game, uuid);
			FRWTable lst_elm = new FRWTable(GameConstants.DEFAULT_TBL_STYLE, GameConstants.getUIColor());

			FRWTable tbl_img = new FRWTable();

			tbl_img.add(cha.getFacing(FRWCharacter.FRONT)).size(32).uniformX().row();

			TextButton btn = new FRWTextButton("Load");
			btn.setName(uuid.toString());
			btn.addListener(listener);
			btn.pad(2).padLeft(8).padRight(8);

			tbl_img.add(btn).left();
			
			lst_elm.add(tbl_img).expandX().left();
			return lst_elm;
		} catch (IOException e) {
			Gdx.app.error(SaveLoadState.class.getName(), "Could not load character", e);
		}
		return null;
	}
	
	class SLListener extends ClickListener {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			
			Gdx.app.debug(SLListener.class.getName(), event.getListenerActor().getName());
		}
	}
}