package org.nrs.state;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.frw.debug.DebugConsole;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * AGameState is an abstract class that implements Screen and takes care of the
 * basic functions of creating and running a "Game State" for this Game.
 * 
 * @author Cole Pram
 */
public abstract class AGameState implements Screen {

	float pole = 0;
	float poleTime = 0.25f;
	
	/**
	 * game holds an implementation of the main game and can be called
	 * by extending classes to acquire common elements such as the SpriteBatch
	 * and Camera
	 */
	protected final FantasyRunWild game;
	
	/**
	 * Most actors should be added to the stage and the stage should be allowed
	 * to control most of the rendering and updating.
	 */
	protected DebugableStage stage;

	public AGameState(final FantasyRunWild game) {
		this.game = game;
		
		stage = new DebugableStage(new FitViewport(GameConstants.WIDTH, GameConstants.HEIGHT, game.cam));
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.25f, 0.25f, 0.25f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		render(game.batch);

		update(delta);
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
	}

	@Override
	public void pause() {/*override if needed*/}

	@Override
	public void resume() {/*override if needed*/}

	@Override
	public void hide() {/*override if needed*/}

	/**
	 * update is called in the AGameState render method. Before
	 * render(batch) is called
	 * 
	 * @param delta
	 */
	public void update(float delta) {
		pole+= poleTime;
		if( pole > 1 ) {
			pole -= 1;
			poleDebug();
		}
		stage.act(delta);
	}

	/**
	 * poleDebug updates the Debug console with any information this state wants to display for debugging purposes
	 */
	protected void poleDebug() {
		stage.console.setText("Hello\nworld");
	}
	
	/**
	 * render is called in the AGameState render method. After
	 * update(delta) is called.
	 * 
	 * @param batch
	 */
	public void render(SpriteBatch batch) {
		stage.draw();
	}

	@Override
	public void dispose() {
		stage.dispose();
		Gdx.input.setInputProcessor(null);
	}

	class DebugableStage extends Stage {
		
		private final Color debugColor = new Color(1.0f, 0, 0, 0.85f);
		
		DebugConsole console = new DebugConsole(game);
		
		public DebugableStage(Viewport view) {
			super(view);
			console.setOriginY(getHeight());
		}
		
		@Override
		public void draw() {
			super.draw();
			
			Batch batch = getBatch();
			batch.begin();
			console.draw(batch, 1);
			batch.end();
		}
		
		@Override
		public Color getDebugColor() {
			return debugColor;
		}
	}
}
