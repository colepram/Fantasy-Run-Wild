package org.nrs.state;

import org.nrs.FantasyRunWild;
import org.nrs.frw.character.TestPlayer;
import org.nrs.frw.map.LocalMap;
import org.nrs.frw.map.MapData;
import org.nrs.frw.map.SessionManagerInterface;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;

public class LocalMapState extends AGameState {

	SessionManagerInterface ttm;
	MapData data;
	LocalMap map;
	TestPlayer player;
	
	public LocalMapState(FantasyRunWild game) {
		super(game);
		
		ttm = new TempTileManager();
		data = new MapData(ttm);
		
		map = new LocalMap(stage, ttm, data);

		player = new TestPlayer(game, ttm);
		
		//set the current world location to the center of the world
		player.setPosition( map.getWidth()/2, map.getHeight()/2);
		
		//set the player to the middle of the screen
		player.setOrigin( stage.getWidth()/2, stage.getHeight()/2);
	
		map.setPlayer(player);
		
		stage.addActor(map);
		stage.addActor(player);
		
		stage.setDebugAll(true);
	}
	
	/**
	 * This is a temporary example of how a TileManager works. It loads the textures used for a tile
	 * into some kind of ID referenced data structure and returns the tile requested by the calling
	 * MapChunk. 
	 * 
	 * @author colepram
	 */
	public class TempTileManager implements SessionManagerInterface {
		//size of a tile in pixels
		private int tileSize = 16;
		
		//number of tiles that should be buffered
		private int tileRange = 1;

		//in order ids of tiles that are handled
		int [] tileIDs = {0,1,2,3,4};

		//tile Textures
		private Texture [] tileManager;
		
		private int mapSize = 6;
		private int mapSeed = -1;//53998;
		
		public TempTileManager() {
			tileManager = new Texture[tileIDs.length];
			
			for( int i=0; i<tileIDs.length; i++) {
				Pixmap pixmap = new Pixmap(tileSize, tileSize, Format.RGB888);
				Color c = null;
				
				switch(tileIDs[i]) {
				case 0: c = Color.BLUE;	break;
				case 1: c = Color.SKY; break;
				case 2: c = Color.BROWN; break;
				case 3: c = Color.GREEN; break;
				case 4: c = Color.FOREST; break;
				default: c = Color.PURPLE;
				}
				
				pixmap.setColor(c);
				pixmap.fillRectangle(0, 0, tileSize, tileSize);
				
				tileManager[i] = new Texture(pixmap);
			}
		}
		
		@Override
		public int getTileSize() {
			return tileSize;
		}
		
		@Override
		public Texture getTile(int tileID) {
			return tileManager[tileID];
		}

		@Override
		public int getMaxTileId() {
			return tileIDs.length - 1;
		}
		
		public int getBufferRange() {
			return tileRange;
		}

		@Override
		public int getMapSize() {
			return mapSize;
		}

		@Override
		public int getMapSeed() {
			return mapSeed;
		}
	}
	
	@Override
	protected void poleDebug() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Player: [");
		sb.append(player.getX());
		sb.append(",");
		sb.append(player.getY());
		sb.append("]\n");
		
		sb.append("Chunk: [");
		sb.append(map.getXChunkIndex());
		sb.append(",");
		sb.append(map.getYChunkIndex());
		sb.append("] ");
		
		int [] tile = map.getTileXY();
		
		sb.append("Tile: [");
		sb.append(tile[0]);
		sb.append(",");
		sb.append(tile[1]);
		sb.append("]");
		stage.console.setText(sb.toString());
	}
}
