package org.nrs.state;

import java.io.IOException;
import java.util.UUID;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.frw.character.FRWCharacter;
import org.nrs.res.NameGenerator;
import org.nrs.ui.FRWButton;
import org.nrs.ui.FRWCheckBox;
import org.nrs.ui.FRWImageButton;
import org.nrs.ui.FRWScrollPane;
import org.nrs.ui.FRWTable;
import org.nrs.ui.FRWTextButton;
import org.nrs.ui.FRWTextField;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class CharacterGeneratorState extends AGameState {

	private FRWTable tbl_presets;
	private FRWTable tbl_sprite;
	private FRWTable tbl_name;
	private FRWTable tbl_buttons;

	private Button layerComponent;
	
	private TextField nameField;
	private ButtonGroup<CheckBox> cbg = new ButtonGroup<CheckBox>();

	private String[] facingNames = { "front", "back", "left", "right" };

	private FRWCharacter player;

	float accum = 0;

	private UUID curUUID;
	
	public CharacterGeneratorState(final FantasyRunWild game) {
		super(game);

		player = new FRWCharacter(game);
		loadSprites();

		FRWTable tDesign = new FRWTable();
		tDesign.setFillParent(true);
		tDesign.add(getNameTable()).fillX().row();
		
		tDesign.add(getPresetsTable()).fillX().row();
		tDesign.add(getSpriteTable()).fillX();
		// tDesign.add(getButtons()).expandX().fillX();
		tDesign.pack();
		stage.addActor(tDesign);
	}

	private Actor getButtons() {
		if (tbl_buttons == null) {
			tbl_buttons = new FRWTable();
			Button btnSave = new FRWTextButton("Save");
			btnSave.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					try {
						curUUID = player.saveCharacter();
					} catch (Exception e) {
						Gdx.app.error("CharacterGeneratorState: getButtons", "Could not save player");
						e.printStackTrace();
					}
				}
			});
			tbl_buttons.add(btnSave).expand().fillX().row();

			Button btnLoad = new FRWTextButton("Load");
			btnLoad.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					try {
						if( curUUID != null ) {
							player.loadCharacter(curUUID);
							getNameField().setProgrammaticChangeEvents(false);
							getNameField().setText(player.getName());
							getNameField().setProgrammaticChangeEvents(true);
						}
					} catch (IOException e) {
						Gdx.app.error("CharacterGeneratorState: getButtons", "Could not load player");
						e.printStackTrace();
					}
				}
			});
			tbl_buttons.add(btnLoad).expand().fillX();

		}
		return tbl_buttons;
	}

	private Actor getNameTable() {
		if (tbl_name == null) {
			tbl_name = new FRWTable();

			tbl_name.add(getNameField()).growX();
			
			Button btn = new FRWImageButton();
			
			// btn.setBackground(GameConstants.uiSkin.getDrawable("dice"));
			btn.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);

					getNameField().setText(NameGenerator.getName(NameGenerator.FEMALE));
				}
			});

			tbl_name.add(btn).expandY().fillY();
		}
		return tbl_name;
	}

	private TextField getNameField() {
		if (nameField == null) {
			nameField = new FRWTextField("");
			nameField.setProgrammaticChangeEvents(true);
			nameField.addListener(new ChangeListener() {

				@Override
				public void changed(ChangeEvent event, Actor actor) {
					player.setName(getNameField().getText());
				}
			});

			nameField.setText(NameGenerator.getName(NameGenerator.FEMALE));
		}
		return nameField;
	}

	private Actor getSpriteTable() {
		if (tbl_sprite == null) {
			tbl_sprite = new FRWTable();

			final Actor front = getTableCell(player.getFacing(FRWCharacter.FRONT));
			front.addListener(new ClickListener() {
				int facing = FRWCharacter.FRONT;
				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
					super.enter(event, x, y, pointer, fromActor);
					player.getFacing(FRWCharacter.FRONT).animate("smile", PlayMode.NORMAL);
				}

				@Override
				public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
					super.exit(event, x, y, pointer, toActor);
					player.getFacing(FRWCharacter.FRONT).animate("smile", PlayMode.REVERSED);
				}
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					int old = facing;
					facing = (facing == FRWCharacter.FRONT) ? FRWCharacter.BACK: FRWCharacter.FRONT;

					((Table) front).getCell(player.getFacing(old)).setActor(player.getFacing(facing));
				}
			});

			final Actor side = getTableCell(player.getFacing(FRWCharacter.LEFT));
			side.addListener(new ClickListener() {
				int facing = FRWCharacter.LEFT;

				@Override
				public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
					super.enter(event, x, y, pointer, fromActor);
					player.getFacing(facing).animate("smile", PlayMode.NORMAL);
				}

				@Override
				public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
					super.exit(event, x, y, pointer, toActor);
					player.getFacing(facing).animate("smile", PlayMode.REVERSED);
				}

				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					int old = facing;
					facing = (facing == FRWCharacter.LEFT) ? FRWCharacter.RIGHT : FRWCharacter.LEFT;

					((Table) side).getCell(player.getFacing(old)).setActor(player.getFacing(facing));
				}
			});

			tbl_sprite.add(getLayerComponent()).grow();
			tbl_sprite.add(front).grow();
			tbl_sprite.add(side).grow();
			tbl_sprite.add(getButtons()).expandX().fillX();

		}
		return tbl_sprite;
	}

	private Button getLayerComponent() {
		if( layerComponent == null ) {
			
			Image img = new Image();
			
			layerComponent = new FRWButton();
			layerComponent.setBackground("default-btn");
			layerComponent.addListener(new SpriteLayerChangeListener());
			layerComponent.add(img);
		}
		
		return layerComponent;
	}

	private Actor getTableCell(Actor actor) {
		FRWTable tc1 = new FRWTable("default-btn", GameConstants.getUIColor());
		tc1.add(actor).growY();

		return tc1;
	}

	private String atlasName;
	private void loadSprites() {

		atlasName = "data/images-assets/human.atlas";
		for (int facing = 0; facing < 4; facing++) {
			player.addLayer(facing, atlasName, "0_" + facingNames[facing] + "_body", "body");
			player.addLayer(facing, atlasName, "0_" + facingNames[facing] + "_cloths", "cloths");
			player.addLayer(facing, atlasName, "0_" + facingNames[facing] + "_hair", "hair");

			if (facing != FRWCharacter.BACK) {
				String eyeLayer = "0_" + facingNames[facing] + "_eyes";
				player.addLayer(facing, atlasName, eyeLayer, "eyes");
				player.addAnimation(facing, atlasName, eyeLayer, "smile", "eyes");
			}
			// create the basic layers for a facing.
			// sprite.addLayer(BASE_LAYER.BODY.label);
			// sprite.addLayer(BASE_LAYER.CLOTHS.label);
			// sprite.addLayer(BASE_LAYER.HAIR.label);
			// sprite.addLayer(BASE_LAYER.EYES.label);

		}
	}

	@Override
	public void update(float delta) {
		super.update(delta);

		accum += delta;
		player.getFacing(FRWCharacter.FRONT).act(accum);
		player.getFacing(FRWCharacter.RIGHT).act(accum);
		player.getFacing(FRWCharacter.LEFT).act(accum);
		player.getFacing(FRWCharacter.BACK).act(accum);
	}

	protected Table getPresetsTable() {
		if (tbl_presets == null) {

			tbl_presets = new FRWTable();

			FRWTable tbl_select = new FRWTable(GameConstants.DEFAULT_TBL_STYLE, GameConstants.getUIColor());

			UpdateLayerStyle uls = new UpdateLayerStyle();
			
			CheckBox cbHair = new FRWCheckBox("Hair");
			cbHair.setName("hair");
			cbHair.addListener(uls);

			CheckBox cbEyes = new FRWCheckBox("Eyes");
			cbEyes.setName("eyes");
			cbEyes.addListener(uls);

			CheckBox cbBody = new FRWCheckBox("Body");
			cbBody.setName("body");
			cbBody.addListener(uls);

			CheckBox cbCloths = new FRWCheckBox("Cloths");
			cbCloths.setName("cloths");
			cbCloths.addListener(uls);

			cbg.add(cbHair);
			cbg.add(cbEyes);
			cbg.add(cbBody);
			cbg.add(cbCloths);

			tbl_select.add(cbHair).grow();
			tbl_select.add(cbEyes).grow();
			tbl_select.add(cbBody).grow();
			tbl_select.add(cbCloths).grow();

			FRWTable tbl_color = new FRWTable(GameConstants.DEFAULT_TBL_STYLE, GameConstants.getUIColor());

			ClickListener listen = new ClickListener() {
				public void clicked(InputEvent event, float x, float y) {
					Color c = event.getTarget().getColor();
					String label = cbg.getChecked().getName();
					player.setLayerColor(label, c);
				};
			};

			float [] colorArray = new float[3];
			int perRow = 15;
			int num_colors = 15;
			int satr = 3;
			int litr = 5;
			
			for( int i=0; i<perRow; i++ ) {
				float c = (float)i / (float)perRow;
				addColorButton(tbl_color, new Color(c,c,c, 1), perRow, listen);
			}
			
			for (int i = 0; i < (num_colors); i++) {
				for( int k=0; k<satr; k++ ) {
					float sat = ((float)((k+1)/(float)satr)*0.8f) + 0.2f;
					for( int j=0; j<litr; j++ ) {
						
						float lit = (((float)(j+1)/(float)litr)*0.8f) + 0.2f;
						
						float val = (float)(i)/(float)(num_colors);
						java.awt.Color hsb = java.awt.Color.getHSBColor(val, sat, lit);
						colorArray = hsb.getRGBComponents(null);
						
						Color c = new Color(colorArray[0], colorArray[1], colorArray[2], 1);
		
						addColorButton(tbl_color, c, perRow, listen);
					}
				}
			}
			tbl_presets.add(tbl_select).expandX().fillX().row();
			
			ScrollPane sp = new FRWScrollPane(tbl_color);
			sp.setFadeScrollBars(false);
			

			tbl_presets.add(sp).expandX().fillX();
		}

		return tbl_presets;
	}

	private void addColorButton(Table table, Color c, int perRow, EventListener listen) {
		Button btn = new Button(GameConstants.uiSkin, GameConstants.DEFAULT_BTN_RECT);
		btn.setColor(c);
		btn.addListener(listen);

		Cell<Button> cell = table.add(btn).size(15, 15);
		if (cell.getColumn() % perRow == perRow - 1) {
			cell.row();
		}
	}

	@Override
	public void dispose() {
		// player.dispose();
	}

	class UpdateLayerStyle extends ClickListener {
			
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);
			super.clicked(event, x, y);
			
			Cell<?> a = getLayerComponent().getCells().get(0);
			Image img = (Image)a.getActor();
			String label = cbg.getChecked().getText().toString().toLowerCase();
			img.setDrawable((new TextureRegionDrawable(player.getFacing(FRWCharacter.FRONT).getLayer(label))));
			a.setActor(img);
		}
	}
	
	class SpriteLayerChangeListener extends ClickListener {
		
		TextureAtlas ta;
		private int counter = 0;
		public SpriteLayerChangeListener() {
			ta = game.manager.get(atlasName, TextureAtlas.class);
		}
		
		@Override
		public void clicked(InputEvent event, float x, float y) {
			super.clicked(event, x, y);

			String label = cbg.getChecked().getName();
			
			String tLayer = player.getTextureLayerFor(FRWCharacter.FRONT, label);
			if( tLayer != null ) {
				String [] tmp = tLayer.split("_");
				counter = Integer.parseInt(tmp[0]);
			}
			
			counter++;
			String curLayer = counter +"_front_" + label;
			AtlasRegion ar = ta.findRegion(curLayer);

			if( ar == null ) {
				counter = 0;
				curLayer = counter +"_front_" + label;
				ar = ta.findRegion(curLayer);
			}
			
			Cell<?> a = getLayerComponent().getCells().get(0);
			Image img = (Image)a.getActor();
			img.setDrawable((new TextureRegionDrawable(ar)));
			a.setActor(img);
			
			player.addLayer(FRWCharacter.FRONT, atlasName, counter +"_front_" + label, label);
			player.addLayer(FRWCharacter.BACK, atlasName, counter +"_back_" + label, label);
			player.addLayer(FRWCharacter.LEFT, atlasName, counter +"_left_" + label, label);
			player.addLayer(FRWCharacter.RIGHT, atlasName, counter +"_right_" + label, label);
		}
	}
}
