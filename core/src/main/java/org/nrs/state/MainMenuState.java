package org.nrs.state;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.ui.FRWTextButton;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class MainMenuState extends AGameState {

	public MainMenuState(final FantasyRunWild game) {
		super(game);

		FRWTextButton btnNew = new FRWTextButton("New Game");
		btnNew.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				AGameState ags = new CharacterGeneratorState(game);
				game.setScreen(ags);
			}
		});
		
		FRWTextButton btnLoad = new FRWTextButton("Load Game");
		btnLoad.addListener(new ClickListener() {
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				
				AGameState ags = new SaveLoadState(game);
				game.setScreen(ags);
			};
		});
//		btnNew.setColor(Color.GOLD);
//		btnNew.getStyle().fontColor = Color.GOLD;
		Table tDesign = new Table(GameConstants.uiSkin);
		tDesign.setFillParent(true);
		tDesign.add(btnNew).row();
		tDesign.add(btnLoad);
		tDesign.pack();
		stage.addActor(tDesign);
	}

}
