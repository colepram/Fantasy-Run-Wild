package org.nrs;

import org.nrs.frw.GameConstants;
import org.nrs.state.CharacterGenState2;
import org.nrs.state.LocalMapState;
import org.nrs.state.MainMenuState;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;


public class FantasyRunWild extends Game {

	public AssetManager manager;
	
	public SpriteBatch batch;
	public OrthographicCamera cam;

	public BitmapFont font;	
	
	@Override
	public void create() {
		Gdx.app.setLogLevel(GameConstants.LOG_LEVEL);
		manager = new AssetManager();
		
		loadFonts();
		loadAssets();
		
		batch = new SpriteBatch();
		cam = new OrthographicCamera();
		cam.setToOrtho(false, GameConstants.WIDTH, GameConstants.HEIGHT);
		font = new BitmapFont();
		
		//this.setScreen(new MainMenuState(this));
		this.setScreen(new CharacterGenState2(this));
	}
	
	private void loadFonts() {
		FreeTypeFontGenerator goudyGen = new FreeTypeFontGenerator(Gdx.files.internal("data/goudy/GoudyMediaeval-Regular.ttf"));
		FreeTypeFontParameter goudyPar = new FreeTypeFontParameter();
		goudyPar.size = 20;
		BitmapFont goudyFont = goudyGen.generateFont(goudyPar);
		goudyFont.setColor(Color.GOLD);

		GameConstants.uiSkin = new Skin();

		GameConstants.uiSkin.add("default-font", goudyFont);
		
		TextureAtlas ta = new TextureAtlas(Gdx.files.internal(GameConstants.skin_location + ".atlas"));
		

		GameConstants.uiSkin.addRegions(ta);
		GameConstants.uiSkin.load(Gdx.files.internal(GameConstants.skin_location+".json"));
		
		
	}

	private void loadAssets() {
		manager.load(GameConstants.ATLAS_HUMAN, TextureAtlas.class);
		manager.load(GameConstants.ATLAS_BLANKS, TextureAtlas.class);
		manager.finishLoading();
	}
	
	@Override
	public void render() {
		super.render();
		
		if( Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
			Gdx.app.exit();
		}
	}
	
	@Override
	public void dispose() {
		batch.dispose();
		font.dispose();
		this.getScreen().dispose();
		manager.dispose();
	}
}