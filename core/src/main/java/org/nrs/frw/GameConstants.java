package org.nrs.frw;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class GameConstants {

	public static final String skin_location = "data/images-assets/uistyle/uistyle";

	public static final int LOG_LEVEL = Application.LOG_DEBUG;

	public static float STEP = 1 / 60f;
	public static final String TITLE = "Fantasy Run Wild!";
	public static final int WIDTH = 480;
	public static final int HEIGHT = 360;
	
	public static final int SCALE = 2;


	public static Skin uiSkin;
	
	public static final String DEFAULT_TBL_STYLE = "default-pnl";
	public static final String DEFAULT_BTN_RECT = "rect";

	public static final String ATLAS_BLANKS = "data/images-assets/blank.atlas";

	public static final String ATLAS_HUMAN = "data/images-assets/human.atlas";

	public static final String FONT = "data/goudy/GoudyMediaeval-Regular.ttf";
	
	public static String SAVE_DIR = "/saves/";

	public static Color getUIColor() {
		return Color.GOLD;
	}
	
}
