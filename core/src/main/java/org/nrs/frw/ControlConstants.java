package org.nrs.frw;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public enum ControlConstants {

	UP(Keys.W),
	DOWN(Keys.S),
	LEFT(Keys.A),
	RIGHT(Keys.D),
	RUN(Keys.SHIFT_LEFT),
	GRID(Keys.G, Keys.CONTROL_LEFT);
	
	public int key;
	public int mod;
	private ControlConstants(int key) {
		this(key, -1);
	}
	
	private ControlConstants(int key, int mod) {
		this.key = key;
		this.mod = mod;
	}

	public boolean isPressed() {
		return Gdx.input.isKeyPressed(key) && (mod == -1 || Gdx.input.isKeyPressed(mod));
	}

	public boolean isJustPressed() {
		return Gdx.input.isKeyJustPressed(key) && (mod == -1 || Gdx.input.isKeyPressed(mod));
	}
}
