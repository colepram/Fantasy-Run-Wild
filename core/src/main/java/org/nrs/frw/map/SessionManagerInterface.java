package org.nrs.frw.map;

import com.badlogic.gdx.graphics.Texture;

public interface SessionManagerInterface {
	
	/**
	 * getTileSize returns the size of a tile in pixels
	 * 
	 * @return
	 */
	public int getTileSize();
	
	/**
	 * getMaxTileId returns the maximum id this tile manager has
	 * access to. It's expected tile ids will be sequential starting at 0
	 * 
	 * @return
	 */
	public int getMaxTileId();
	
	/**
	 * return the stored texture associated with the requested tileID
	 * 
	 * @param tileID
	 * @return
	 */
	public Texture getTile(int tileID);
	
	/**
	 * getBufferRange 
	 * 
	 * @return
	 */
	public int getBufferRange();

	/**
	 * getMapSize returns the power of 2 used to create map data
	 * 
	 * map size is 2^(mapSize) + 1
	 * 
	 * mapSize = 1 - create a map of size 3x3
	 * mapSize = 2 - create a map of size 5x5
	 * mapSize = 3 - create a map of size 9x9
	 * mapSize = 4 - create a map of size 17x17
	 * mapSize = 10 - create a map of size 1025x1025
	 * 
	 * @return
	 */
	public int getMapSize();
	
	/**
	 * getMapSeed returns a value used to seed a map generator.
	 * if -1 the map generator should use a random seed.
	 * 
	 * @return
	 */
	public int getMapSeed();
}
