package org.nrs.frw.map;

import java.util.ArrayList;
import java.util.HashMap;

import org.nrs.frw.ControlConstants;
import org.nrs.frw.character.TestPlayer;
import org.nrs.frw.character.TurnListenerInterface;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class LocalMap extends Actor implements TurnListenerInterface {
	
	Stage stage;
	
	//The tile manager is basically an asset manager that holds the textures
	//of the tiles which can be referenced by an id number
	SessionManagerInterface sessionManager;
		
	//an array to hold the chunks, in game this could be implemented in different
	//data structures that might handle loading and unloading chunks. For now
	//and array is easy to manage and conceptualize.
	//MapChunk [][] chunks = new MapChunk[worldXChunks][worldYChunks];
	
	HashMap<String,MapChunk> chunkList = new HashMap<String,MapChunk>();
	
	TestPlayer player;
	MapData map;
	boolean showGrid = true;
	
	public LocalMap(Stage stage, SessionManagerInterface tm, MapData data) {
		this.sessionManager = tm;
		this.stage = stage;
		this.map = data;
		
		setWidth(data.getXChunks() * data.getChunkWidth() * tm.getTileSize());
		setHeight(data.getYChunks() * data.getChunkHeight() * tm.getTileSize());

		Gdx.app.debug("localMap", "worldChunks [" + data.getXChunks() + ", " + data.getYChunks() + "]");
		Gdx.app.debug("localMap", "worldSize [" + getWidth() + ", " + getHeight() + "]");

		//move the maps origin to the center of the screen
		setOrigin(stage.getWidth()/2, stage.getHeight()/2);
	}
	
	public void setPlayer(TestPlayer player) {
		this.player = player;
		this.player.addTurnListener(this);
		
		setPosition(player.getX(), player.getY());

		updateChunks();
	}
	
	/**
	 * loadChunk retrieves the chunk for a given location. If it's already stored
	 * in the Maps data structure, get it from there. Otherwise create the chunk.
	 * 
	 *  At some point there will have to be a clean up method to remove chunks that
	 *  are outside of the area the user can see them to free up the memory used to
	 *  hold and process the chunk. This is a simple implementation that demonstrates
	 *  creating and storing a chunk, a very basic chunk, in an array.
	 *  
	 * @param xIdx
	 * @param yIdx
	 * @return
	 */
	public MapChunk loadChunk(int xIdx, int yIdx) {
		
		Gdx.app.debug("getChunk", "[" + getX() + ", " + getY() + "] [" + xIdx + ", " + yIdx + "]");

		//initialize the chunk and set its position
		//This is where the tiles for a specific chunk will be loaded,
		//currently using a hardcoded predefined tile array
		MapChunk chunk = new MapChunk("[" + xIdx + "," + yIdx + "]", sessionManager, map.getChunk(xIdx, yIdx));

		//compute the world location of the chunk
		float ox = (int) ( ((float)xIdx / (float)map.getXChunks()) * getWidth()) - sessionManager.getTileSize();
		float oy = (int) ( ((float)yIdx / (float)map.getYChunks()) * getHeight() - sessionManager.getTileSize());
		
		Gdx.app.debug("getChunk", "origin [" + ox + ", " + oy + "]");
		
		chunk.setPosition(ox, oy);
		
		Gdx.app.debug("getChunk", "");
		return chunk;
	}
	
	//This is temporary to simulate a user moving
	
	@Override
	public void act(float delta) {
		super.act(delta);

		if( ControlConstants.GRID.isJustPressed() ) {
			showGrid = !showGrid;
		}
		
		if( player != null ) {
		
			player.act(delta);
			
			if( player.getX() > getWidth() ) {
				player.moveBy(-getWidth(), 0);
				updateChunks();
			} else if( player.getX() < 0 ) {
				player.moveBy( getWidth(), 0);
				updateChunks();
			}
		
			if( player.getY() > getHeight() ) {
				player.moveBy(0, -getHeight());
				updateChunks();
			} else if( player.getY() < 0 ) {
				player.moveBy(0, getHeight());
				updateChunks();
			}
		}
	}

	public int getXChunkIndex() {
		return (int)(map.getXChunks() * (getX() / getWidth())); 
	}

	public int getYChunkIndex() {
		return (int)(map.getYChunks() * (getY() / getHeight()));
	}
	
	public int [] getTileXY() {
		String key = getXChunkIndex() + ":" + getYChunkIndex();
		
		MapChunk chunk = chunkList.get(key);
		if( chunk == null ) {
			return new int[2];
		}
		int [] tilexy = {chunk.getTileX(player.getX()), chunk.getTileY(player.getY())};
		return tilexy;
	}
	
	public void updateChunks() {
		//update teh map with the players position
		setPosition(player.getX(), player.getY());
		
		//get the index of the chunk at the center of the map
		int xIdx = getXChunkIndex(); 
		int yIdx = getYChunkIndex();

		int range = sessionManager.getBufferRange();
		int chunkBuffer = (int)Math.pow((range*2)+1, 2) * 2;

		if( chunkList.size() > chunkBuffer ) {
			Gdx.app.debug("Update Chunks", "Removing Chunks: " + chunkList.size());
			ArrayList<String> remove = new ArrayList<String>();
			
			for( String key : chunkList.keySet() ) {
				String [] idx = key.split(":");
				int cxIdx = Integer.parseInt(idx[0]);
				int cyIdx = Integer.parseInt(idx[1]);
				
				if( cxIdx > xIdx+range || cxIdx < xIdx-range ||
						cyIdx > yIdx+range || cyIdx < yIdx-range) {
					remove.add(key);
				}
			}
			
			for( String key : remove ) {
				chunkList.remove(key);
			}
		}
		
		for( int iy = range; iy >= -range; iy-- ) {
			for( int ix = -range; ix <= range; ix++ ) {
				float worldXOffset = 0;
				float worldYOffset = 0;

				int xChunk = xIdx+ix;
				int yChunk = yIdx+iy;
				
				//handle wrap around of chunks on the x-axis
				if( xChunk < 0 ) {
					xChunk += map.getXChunks();
					worldXOffset = -getWidth();
				} else if( xChunk > map.getXChunks()-1 ) {
					xChunk -= map.getXChunks();
					worldXOffset = getWidth();
				}

				//handle wrap around of chunks on the y-axis
				if( yChunk < 0 ) {
					yChunk += map.getYChunks();
					worldYOffset = -getHeight();
				} else if( yChunk > map.getYChunks()-1 ) {
					yChunk -= map.getYChunks();
					worldYOffset = getHeight();
				}
				
				String chunkKey = xChunk+":"+yChunk;
				if( !chunkList.containsKey(chunkKey) ) {
					Gdx.app.debug("Update Chunks", "Adding Chunks: " + chunkList.size());
					chunkList.put(chunkKey, loadChunk(xChunk, yChunk));
				}
				
				chunkList.get(chunkKey).setOrigin(worldXOffset, worldYOffset);
			}
		}		
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		
		//translate current world coordinates into screen coordinates
		float xOffset = -getX() + getOriginX();
		float yOffset = -getY() + getOriginY();
		
		Matrix4 transform = batch.getTransformMatrix();
		transform.translate(xOffset, yOffset, 0);
		batch.setTransformMatrix(transform);
		
		//draw the currently loaded chunks
		for( MapChunk chunk : chunkList.values() ) {
			chunk.draw(batch, parentAlpha, showGrid);
		}
		
		transform.translate(-xOffset, -yOffset, 0);
		batch.setTransformMatrix(transform);
	}
	
	@Override
	public void drawDebug(ShapeRenderer shapes) {
		//translate current world coordinates into screen coordinates
		float xOffset = -getX()+getOriginX()-sessionManager.getTileSize()/2;
		float yOffset = -getY()+getOriginY();
		
		Matrix4 transform = shapes.getTransformMatrix();
		transform.translate(xOffset, yOffset, 0);
		shapes.setTransformMatrix(transform);

		//super.drawDebug(shapes);
		//draw the currently loaded chunks
		for( MapChunk chunk : chunkList.values() ) {
			chunk.drawDebug(shapes);
		}

		transform.translate(-xOffset, -yOffset, 0);
		shapes.setTransformMatrix(transform);
	}

	@Override
	public void takeTurn() {
		updateChunks();
	}
}
