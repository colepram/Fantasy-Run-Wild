package org.nrs.frw.map.gen;

import java.util.Random;

public class DS2 {

	private int size;
	private long seed;

	private float min = Float.POSITIVE_INFINITY;
	private float max = Float.NEGATIVE_INFINITY;
	
	protected Random rand;

	private float [][] rawData;
	private boolean wrapped = false;
	private float variance = 0;
	
	public DS2(int pot) {
		this(pot, Long.MIN_VALUE);
	}
	
	public DS2(int pot, long seed) {
		size = (int) (Math.pow(2, pot) + 1);
		rawData = new float[size][size];
		
		this.seed = (seed == Long.MIN_VALUE ? (long)(Math.random()*1000000) : seed);

		rand = new Random(this.seed);
		
		for( int y = 0; y < rawData.length; y++ ) {
			for ( int x = 0; x < rawData[y].length; x++ ) {
				rawData[y][x] = Float.NaN;
			}
		}
	}

	public float[][] getData() {
		return rawData;
	}

	protected void seed() {
		if( !wrapped ) {
			seedSquare(0, 0, size-1);
		} else {
			float seedVal = !Float.isNaN(rawData[0][0]) ? rawData[0][0] : 
					!Float.isNaN(rawData[0][size-1]) ? rawData[0][size-1] :
					!Float.isNaN(rawData[size-1][size-1]) ? rawData[size-1][size-1] :
					!Float.isNaN(rawData[size-1][0]) ? rawData[size-1][0] :
						rand.nextFloat();
					
			rawData[0][0] = rawData[0][size-1] = rawData[size-1][size-1] = rawData[size-1][0] = seedVal;

			adjustMinMax(seedVal);
		}
	}
	
	private void adjustMinMax(float val) {
		if( Float.isNaN(val) ) {
			return;
		}
		
		min = val < min ? val : min;
		max = val > max ? val : max;
	}
	
	protected void seedSquare(int x, int y, int size) {
		rawData[y][x] = Float.isNaN(rawData[y][x]) ? rand.nextFloat() : rawData[y][x];
		adjustMinMax(rawData[y][x]);
		
		rawData[y][x+size] = Float.isNaN(rawData[y][x+size]) ? rand.nextFloat() : rawData[y][x+size];
		adjustMinMax(rawData[y][x+size]);
		
		rawData[y+size][x+size] = Float.isNaN(rawData[y+size][x+size]) ? rand.nextFloat() : rawData[y+size][x+size];
		adjustMinMax(rawData[y+size][x+size]);
		
		rawData[y+size][x] = Float.isNaN(rawData[y+size][x]) ? rand.nextFloat() : rawData[y+size][x];
		adjustMinMax(rawData[y+size][x]);
		
	}
	
	public void generate() {
		seed();
		generate(0, 0, size);
	}
	
	public void generate(int xStart, int yStart, int size) {
		
		seedSquare(xStart, yStart, size-1);
		for( int length=size-1, step=(length+1)/2; length>=2; length=step, step=(length+1)/2) {
			square(length, step);
			diamond(length, step);
		}
	}

	public long getSeed() {
		return seed;
	}	

	public void setData(float[][] input) {

		rawData = input;
		
		for( float [] r : rawData ) {
			for( float val : r ) {
				adjustMinMax(val);
			}
		}
	}

	public void square() {
		square(size-1, size/2);
	}
	
	public void square(int length, int mid) {
		for( int x=mid; x<size-1; x+=length ) {
			for( int y=mid; y<size-1; y+=length) {
				square(x, y, mid);
			}
		}
	}

	protected void square(int x, int y, int mid) {
		if( Float.isNaN( rawData[y][x] ) ) {
			rawData[y][x] = (rawData[y-mid][x-mid] + rawData[y+mid][x-mid] +
					rawData[y-mid][x+mid] + rawData[y+mid][x+mid])/4;
			
			rawData[y][x] += getRand(mid);
		}

		adjustMinMax(rawData[y][x]);
	}
	
	public void diamond() {
		diamond(size-1, size/2);
	}
	
	protected void diamond(int l, int m) {
		for( int x=0; x < (wrapped ? size-1 : size); x+=m ) {
			for( int y=(x+m)%l; y<(wrapped ? size-1 : size); y+=l ) {
				if( wrapped ) {
					diamondWrapped(x, y, m);
				} else {
					diamondNoWrap(x, y, m);
				}
			}
		}

	}
	
	protected float diamondWrapped(int x, int y, int mid) {
		rawData[y][x] = rawData[y][(x-mid+(size-1))%(size-1)];
		rawData[y][x] += rawData[y][(x+mid)%(size-1)];
		rawData[y][x] += rawData[(y-mid+(size-1))%(size-1)][x];
		rawData[y][x] += rawData[(y+mid)%(size-1)][x];
		rawData[y][x] /= 4.0f;
		rawData[y][x] += getRand(mid);
		
		adjustMinMax(rawData[y][x]);
		
		//for wrap
		if( x==0 ) rawData[y][size-1] = rawData[y][x];
		if( y==0 ) rawData[size-1][x] = rawData[y][x];
		
		return rawData[y][x];
	}
	
	protected void diamondNoWrap(int x, int y, int m) {
		
		if( Float.isNaN( rawData[y][x] ) ) {
			float sum = 0;
			int n = 0;
			if( x >= m) { 
				sum += rawData[y][x-m];
				n++;
			}
			if( x+m <= size-1 ) {
				sum += rawData[y][x+m];
				n++;
			}
			if( y >= m) {
				sum += rawData[y-m][x];
				n++;
			}
			if( y+m <= size-1 ) {
				sum += rawData[y+m][x];
				n++;
			}
			
			rawData[y][x] = sum/n + getRand(m);
		}
		
		adjustMinMax(rawData[y][x]);
	}
	
	protected float getRand(int size) {
		float var = (float)(size*variance) / (float)getData().length;
		return (rand.nextFloat()*2*var)-var;
	}

	public float getMin() {
		return min;
	}

	public float getMax() {
		return max;
	}

	public void setWrapped(boolean wrapped) {
		this.wrapped  = wrapped;
	}

	public void setVariance(float variance) {
		this.variance  = variance;
	}

	public int getSize() {
		return size;
	}
}
