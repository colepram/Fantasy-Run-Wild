package org.nrs.frw.map;

import org.nrs.frw.map.gen.DiamondSquare;

public class MapData {

	//temporary, this is what a chunk looks like with the id number
	//of the tile at a specific position
	int [][] tiles = {{0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
			{0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0},
			{0,1,0,1,0,2,2,2,2,2,0,1,0,1,0,1},
			{1,0,1,0,1,2,0,0,0,2,1,0,1,0,1,0},
			{0,1,0,1,0,2,0,2,0,2,0,1,0,1,0,1},
			{1,0,1,0,1,2,0,0,0,2,1,0,1,0,1,0},
			{0,1,0,1,0,2,0,2,0,2,0,1,0,1,0,1},
			{1,0,1,0,1,2,2,2,2,2,1,0,1,0,1,0},
			{0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1},
			{1,0,1,0,1,0,1,0,1,0,1,0,1,0,1,0}};
	
	private int chunk = 16;
	private int chunkWidth = chunk;
	private int chunkHeight = chunk;

	//how many chunks are in this world, in game I'd like this to be a really
	//large number so a user isn't able to easily just walk around the world.
	private int xChunks = 25;
	private int yChunks = 25;
	
	float [][] tileData;

	SessionManagerInterface tmi;
	
	public MapData(SessionManagerInterface tmi) {
		this.tmi = tmi;
		
		tileData = new float[yChunks*chunkHeight][xChunks*chunkWidth];

		int pot = tmi.getMapSize();

		int size = (int) Math.pow(2, pot) + 1;
		xChunks = yChunks = size/chunkWidth;

		DiamondSquare ds = new DiamondSquare(pot);
		
		tileData = tmi.getMapSeed()==-1 ? ds.generate() : ds.generate(tmi.getMapSeed());
		
		DiamondSquare.writeImage(ds.getSeed() + "_image.png", tileData);
	}
	
	public int getXChunks() {
		return xChunks;
	}
	
	public int getYChunks() {
		return yChunks;
	}
	
	public int getChunkWidth() {
		return chunkWidth;
	}
	
	public int getChunkHeight() {
		return chunkHeight;
	}
	
	public int [][] getChunk(int xIdx, int yIdx) {
		int [][] chunk = new int[getChunkHeight()][getChunkWidth()];
		for( int y = 0; y<getChunkHeight(); y++ ) {
			for( int x = 0; x<getChunkWidth(); x++ ) {
				int iyIdx = (yChunks-1-yIdx)*getChunkHeight() + (getChunkHeight()-1)-y;
				int ixIdx = xIdx*getChunkWidth() + x;
				
				chunk[y][x] = (int)(tmi.getMaxTileId() * tileData[ixIdx][iyIdx]);
			}
		}
		return chunk;
	}
}
