package org.nrs.frw.map.gen.voronoi;

public class FSite extends FPoint {

	public static int sitesCreated = 0;
	
	private double val;
	
	public FSite(double x, double y, double val) {
		super(x, y);
		this.val = val;
		sitesCreated++;
	}

	public double getValue() {
		return val;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("val: ");
		sb.append(val);
		sb.append(" [");
		sb.append(getX());
		sb.append(",");
		sb.append(getY());
		sb.append("]");
		
		return sb.toString();
	}
}
