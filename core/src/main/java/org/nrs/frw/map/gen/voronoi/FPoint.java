package org.nrs.frw.map.gen.voronoi;

public class FPoint implements Comparable<FPoint> {

	public static int pointsCreated = 0;
	
	private double x, y;
	
	public FPoint(double x, double y) {
		this.x = x;
		this.y = y;
		pointsCreated++;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

	@Override
	public int compareTo(FPoint o) {
		return y < o.getY() ? -1 : y > o.getY() ? 1 : x < o.getX() ? -1 : x > o.getX() ? 1 : 0;
	}

	public String toString() {
		return "[" + x + "," + y + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof FPoint) ? this.compareTo((FPoint)obj) == 0 : super.equals(obj);
	}

	public double distance(FPoint end) {
		double x = end.getX()-this.x;
		double y = end.getY()-this.y;
		
		return Math.sqrt(x*x + y*y);
	}

	public void setXY(double x, double y) {
		this.x = x;
		this.y = y;
	}
}
