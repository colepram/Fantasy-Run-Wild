package org.nrs.frw.map.gen;

import org.nrs.frw.map.SessionManagerInterface;

public class MapData2 {

	private SessionManagerInterface sessionManager;

	private float [][] mapData;
	
	public MapData2(SessionManagerInterface sessionManager) {
		this.sessionManager = sessionManager;

		DiamondSquare generator = new DiamondSquare(sessionManager.getMapSize());		
		mapData = generator.generate(sessionManager.getMapSeed());
	}
	
	public float [][] getMapData() {
		return mapData;
	}

	public int getYChunks() {
		return mapData.length / 3;
	}
	
	public int getXChunks() {
		return mapData.length / 3;
	}
	
	public float[][] getChunk(int xIdx, int yIdx) {
		
		float [][] chunk;

		float [][] init = new float[3][3];
		for( int y = 0; y<init.length; y++ ) {
			for( int x = 0; x<init[0].length; x++ ) {
				init[y][x] = getMapData()[yIdx*3+y][xIdx*3+x];
			}
		}
		
		DiamondSquare ds = new DiamondSquare(sessionManager.getMapSize());
		ds.setWrapped(false);
		ds.setInit(init, 0.05f);
		chunk = ds.generate(sessionManager.getMapSeed());

		return chunk;
	}
}
