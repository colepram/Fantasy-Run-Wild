package org.nrs.frw.map.gen;

import java.util.Random;

public class MapGen {

	private int size;
	private int subsizeModifier = 2;
	private float [][] grid;
	
	private Random rand;
	
	public MapGen(int pot) {
		size = (int) Math.pow(2, pot) + 1;
		grid = new float[size][size];
		rand = new Random();
		
		for( int y=0; y<grid.length; y++ ) {
			for( int x=0; x<grid[y].length; x++ ) {
				grid[y][x] = Float.NaN;
			}
		}
	}
	
	public float[][] getGrid() {
		return grid;
	}

	public int getSubSize() {
		return getPoT(size)/subsizeModifier;
	}
	
	public int getPoT(int size) {
		return (int) ((Math.log(size-1)/Math.log(2)));
	}

	public int getSize() {
		return size;
	}

	public void spacePoints(float [][] input) {
		//figure out how many points we have to place on our grid
		int nPoints = input.length * input[0].length;
		
		//compute the maximum allowable distance between points.
		float maxAllowableDistance = ((float)size-1)/((float)input.length-1);
		
		int plusMinus = (int) maxAllowableDistance/2;

		//place the points on the grid with some random tolerance
		//of their evenly spaced values
		for( int y=0; y<input.length; y++ ) {
			int ypos = (int) Math.ceil(y*maxAllowableDistance);
			for( int x=0; x<input[y].length; x++ ) {
				int xpos = (int) Math.ceil(x*maxAllowableDistance);
				
				//compute an offset, if we're at x==0 or x==input.length-1 then subtract 0.5
				//to get a value that's +/- 50% of the allowable distance.
				float xRand = rand.nextFloat();
				float yRand = rand.nextFloat();
				xRand = (x <= 0 ? xRand*0.5f : x >= input.length-1 ? xRand*(-0.5f) : xRand-0.5f);
				yRand = (y <= 0 ? yRand*0.5f : y >= input.length-1 ? yRand*(-0.5f) : yRand-0.5f);

				int xOffset = (int)Math.round(plusMinus*xRand);
				int yOffset = (int)Math.round(plusMinus*yRand);
				
				grid[ypos+yOffset][xpos+xOffset] = input[y][x];
			}
		}
	}

}
