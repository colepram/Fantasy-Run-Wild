package org.nrs.frw.map.gen.voronoi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.PriorityQueue;

import com.badlogic.gdx.math.Circle;

/**
 * 
 * Q = priortiyQueue of events T = beach line (binary tree of sites)
 * 
 * Load sites into Q
 * 
 * p <- Q.delete if( p instanceof site ) { q <- find nearest site to p r <- find
 * nearest site to q if( q != null && r != null ) { b = find bisector of p, q, r
 * d = create circle event (p, q, r, point(b.x, b.y + dist(b, p))) Q.add(d) }
 * T.add(p) } else { //if p instanceof circle }
 * 
 * @author colepram
 *
 */
public class Fortune {

	public static int sitesProcessed = 0;
	public static int circlesProcessed = 0;
	
	private PriorityQueue<FPoint> queue;
	private float width, height;

	private Collection<FEdge> edges;
	private Collection<FSite> sites;
	private Collection<FCircle> circles;

	private FCurve root = null;

	private SiteEventListener sel;
	
	public Fortune(SiteEventListener sel) {
		this.sel = sel;
		this.sel.setFortune(this);
		queue = new PriorityQueue<>();
		edges = new ArrayList<>();
		sites = new ArrayList<>();
		circles = new ArrayList<>();
	}

	public FCurve getRoot() {
		return root;
	}

	public PriorityQueue<FPoint> getQueue() {
		return queue;
	}

	public Collection<FEdge> getEdges() {
		return edges;
	}

	public Collection<FSite> getSites() {
		return sites;
	}

	public Collection<FCircle> getCircles() {
		return circles;
	}
	
	public int loadQueue(float[][] input) {

		width = input[0].length;
		height = input.length;

		for (int y = 0; y < input.length; y++) {
			for (int x = 0; x < input[y].length; x++) {
				if (!Float.isNaN(input[y][x])) {
					queue.add(new FSite(x, y, input[y][x]));
				}
			}
		}

		return queue.size();
	}

	public void processPoints() {
		while( !queue.isEmpty() ) {
			FPoint p = queue.remove();
			if( p instanceof FSite ) {
				processSite((FSite)p);
				sites.add((FSite)p);
			} else {
				processCircle((FCircle)p);
			}
			sel.siteProcessed();
		}
		
		closeEdges();
	}

	private void closeEdges() {
		for( FEdge e : edges ) {
			if( e.getEnd() == null ) {
				e.closeEdge(width, height);
			}
		}
	}
	
	public void processCircle(FCircle event) {
		double curY = event.getY();
		
		if( event.isDeleted() ) {
			return;
		}
		
		FCurve leftParent = event.getCurve().getLeftParent();
		FCurve rightParent = event.getCurve().getRightParent();
		FCurve leftSide = leftParent.getLargestLeft();
		FCurve rightSide = rightParent.getSmallestRight();
		
		if( leftSide.getCircle() != null ) {
			leftSide.getCircle().setDeleted(true);
		}
		
		if( rightSide.getCircle() != null ) {
			rightSide.getCircle().setDeleted(true);
		}
		
		FPoint bisect = event.getCurve().getBisectingPoint(event.getX(), curY);
		
		leftParent.getEdge().setEnd(bisect);
		rightParent.getEdge().setEnd(bisect);
		
		FCurve last = event.getCurve();
		FCurve parent = null;
		while( last != root && last.getParent() != null) {
			last = last.getParent();
			if( last == leftParent ) {
				parent = leftParent;
			} else if( last == rightParent ) {
				parent = rightParent;
			}
		}

		FEdge edge = new FEdge(bisect, leftSide.getSite(), rightSide.getSite());
		parent.setEdge(edge);
		edges.add(edge);
		
		event.getCurve().remove();
		event.setDeleted(true);
		
		circleCheck(leftSide, curY);
		circleCheck(rightSide, curY);
		
		circlesProcessed++;
	}

	public void processSite(FSite event) {
		double curY = event.getY();
		if( root == null ) {
			root = new FCurve(event);
			sitesProcessed++;
			return;
		}
		
		if( root.isLeaf() && curY == root.getSite().getY() ) {
			FCurve left = root;
			FCurve right = new FCurve(event);
			
			double x = (left.getSite().getX()+right.getSite().getX())/2;

			FPoint start = new FPoint(x, curY);
			FEdge edge = new FEdge(start, left.getSite(), right.getSite());
			
			FCurve nRoot = new FCurve(start);
			nRoot.setEdge(edge);
			nRoot.setLeft(left);
			nRoot.setRight(right);
			root = nRoot;
			
			edges.add(edge);
			sitesProcessed++;
			return;
		}
		
		FCurve node = root.findSite(event.getX(), curY);
		
		if( node.getCircle() != null ) {
			node.getCircle().setDeleted(true);
			node.setCircle(null);
		}
		
		FPoint start = node.getBisectingPoint(event.getX(), curY);

		FEdge leftEdge = new FEdge(start, node.getSite(), event);
		FEdge rightEdge = new FEdge(start, event, node.getSite());
		leftEdge.setNeighbour(rightEdge);

		node.setEdge(rightEdge);
		edges.add(leftEdge);
		
		FCurve left = new FCurve(start);
		left.setEdge(leftEdge);
		left.setLeft(new FCurve(node.getSite()));
		left.setRight(new FCurve(event));
		
		node.setLeft(left);
		node.setRight(new FCurve(node.getSite()));
		
		circleCheck(node.getLeft(), curY);
		circleCheck(node.getRight(), curY);

		sitesProcessed++;
	}

	public void circleCheck(FCurve curve, double curY) {
		FCurve leftParent = curve.getLeftParent();
		if( leftParent == null || leftParent.getEdge() == null) {
			return;
		}
		
		FCurve rightParent = curve.getRightParent();
		if( rightParent == null || rightParent.getEdge() == null) {
			return;
		}
		
		FCurve leftSide = leftParent.getLargestLeft();
		FCurve rightSide = rightParent.getSmallestRight();
		
		if( leftSide == null || rightSide == null || leftSide.getSite().equals(rightSide.getSite())) {
			return;
		}
		
		if( leftSide.getSite().getY() == curY && rightSide.getSite().getY() == curY && curve.getSite().getY() == curY) {
			return;
		}
		
		FPoint bisect = leftParent.getEdge().bisect(rightParent.getEdge());
		if( bisect == null ) {
			return;
		}
		
		double x = (leftSide.getSite().getX()-bisect.getX());
		double y = (leftSide.getSite().getY()-bisect.getY());
		double radius = Math.sqrt( x*x + y*y );
		
		if( y+radius <= curY ) {
			return;
		}
		
		//create a new circle if the bisection isn't an instance of
		//a circle
		FCircle circle = new FCircle(bisect.getX(), bisect.getY(), radius);
		
		circle.setCurve(curve);
		
		queue.add(circle);
		circles.add(circle);
	}

	public static abstract class SiteEventListener {
		Fortune f;
		
		public abstract void siteProcessed();
		
		public Collection<FEdge> getEdges() {
			return f.getEdges();
		}

		public Collection<FSite> getSites() {
			return f.getSites();
		}
		
		public Collection<FCircle> getCircles() {
			return f.getCircles();
		}

		public void setFortune(Fortune f) {
			this.f = f;
		}
	}
}
