package org.nrs.frw.map.gen;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

public class DiamondSquare {

	private int size;
	public float [][] data;
	
	public float [] left_seeds;
	public float [] right_seeds;
	public float [] top_seeds;
	public float [] bottom_seeds;
	
	public static final int SEED_LEFT = 0;
	public static final int SEED_RIGHT = 1;
	public static final int SEED_TOP = 2;
	public static final int SEED_BOTTOM = 3;
	
	private Random r;
	private int randomSeed = 100000;
	
	private float min;
	private float max;
	
	private boolean wrap = true;
	private boolean initialized = false;
	
	private float offsetModifier = 1;
	
	public DiamondSquare(int pot) {
		size = (int) Math.pow(2, pot) + 1;
		
		data = new float[size][size];
		for( int y = 0; y<data.length; y++ ) {
			for( int x = 0; x<data[y].length; x++ ) {
				data[x][y] = Float.NaN;
			}
		}
	}
	
	public void setMin(float min) {
		this.min = min;
	}
	
	public void setMax(float max) {
		this.max = max;
	}
	
	public int getSize() {
		return size;
	}

	public void setWrapped( boolean wrapped ) {
		this.wrap = wrapped;
	}
	
	public boolean isWrapped() {
		return wrap;
	}
	
	public void setInit(float[][] init, float offsetMod) {
		this.offsetModifier = offsetMod;
		
		data[0][0] = init[0][0];
		data[0][(size-1)/2] = init[0][1];
		data[0][size-1] = init[0][2];

		data[(size-1)/2][0] = init[1][0];
		data[(size-1)/2][(size-1)/2] = init[1][1];
		data[(size-1)/2][size-1] = init[1][2];

		data[size-1][0] = init[2][0];
		data[size-1][(size-1)/2] = init[2][1];
		data[size-1][size-1] = init[2][2];
		
		initialized = true;
	}

	public float[][] generate() {
		this.randomSeed = (int)(Math.random()*100000.0);
		return generate(this.randomSeed);
	}
	
	public float[][] generate(int seed) {
		this.randomSeed = seed;
		r = new Random(randomSeed);

		if( !initialized ) {
			initialSeed();
		}

		for( int l=size-1, m=(l+1)/2; l>=2; l=m, m=(l+1)/2 ) {
			square(l, m);
			diamond(l, m);
		}
		
		//normalize the values so we always have floats between 0.0 and 1.0
		for( int x=0; x<=size-1; x++ ) {
			for( int y=0; y<=size-1; y++ ){
				data[x][y] = !initialized ? (data[x][y]-min)/(max-min) : data[x][y] < 0 || data[x][y] > 1 ? (data[x][y]-min)/(max-min) : data[x][y];
			}
		}
		
		return data;
	}

	private void square(int l, int m) {
		for( int x=m; x<size-1; x+=l ) {
			for( int y=m; y<size-1; y+=l) {
				square(x, y, m);
			}
		}
	}
	
	private void diamond(int l, int m) {
		for( int x=0; x < (wrap ? size-1 : size); x+=m ) {
			for( int y=(x+m)%l; y<(wrap ? size-1 : size); y+=l ) {
				if( wrap ) {
					diamondWrapped(x, y, m);
				} else {
					diamondNoWrap(x, y, m);
				}
			}
		}

	}

	protected void initialSeed() {
		float initalSeed = 1.0f;

		if( wrap ) {
			data[0][0] = data[0][size-1] = data[size-1][0] = data[size-1][size-1] = 
					getSeeds(size-1, size-1, initalSeed, size);
		} else {
			//check to see if there are preset seeds for the edges
			data[0][0] = getSeeds(0, 0, initalSeed, size); 
			data[0][size-1] =  getSeeds(0, size-1, initalSeed, size);
			data[size-1][0] = getSeeds(size-1, 0, initalSeed, size);
			data[size-1][size-1] = getSeeds(size-1, size-1, initalSeed, size);
		}

		setMinMax(data[0][0]);
		setMinMax(data[size-1][0]);
		setMinMax(data[size-1][size-1]);
		setMinMax(data[0][size-1]);
	}

	private void setSeeds(int side, float[] seedArray) {
		switch(side) {
		case SEED_LEFT:
			left_seeds = seedArray;
			break;
		case SEED_RIGHT:
			right_seeds = seedArray;
			break;
		case SEED_TOP:
			top_seeds = seedArray;
			break;
		case SEED_BOTTOM:
			bottom_seeds = seedArray;
			break;
		default:
			break;
		}
	}
	
	private float getSeeds(int x, int y, float initalSeed, float stepsize) {
		if( !wrap ) {
			if( x <= 0 && left_seeds != null ) {
				return left_seeds[y];
			} else if( x >= size-1 && right_seeds!=null ) {
				return right_seeds[y];
			}
			
			if( y <= 0 && top_seeds != null ) {
				return top_seeds[x];
			} else if( y >= size-1 && bottom_seeds != null) {
				return bottom_seeds[y];
			}
		}
		return initalSeed  + ((r.nextFloat()*2*stepsize)-stepsize)*offsetModifier;
	}

	protected float diamondNoWrap(int x, int y, int m) {
		
		if( Float.isNaN( data[x][y] ) ) {
			int sum = 0;
			int n = 0;
			if( x > m) { 
				sum += getValue(data[x-m][y]);
				n++;
			}
			if( x+m <= size-1 ) {
				sum += getValue(data[x+m][y]);
				n++;
			}
			if( y > m) {
				sum += getValue(data[x][y-m]);
				n++;
			}
			if( y+m <= size-1 ) {
				sum += getValue(data[x][y+m]);
				n++;
			}
			
			data[x][y] = getSeeds(x, y, sum/n, m);
		}
		
		setMinMax(data[x][y]);
		
		return data[x][y];
	}
	
	protected float diamondWrapped(int x, int y, int m) {
		if(Float.isNaN( data[x][y] )) {

			data[x][y] = getValue(data[(x-m+(size-1))%(size-1)][y]);
			data[x][y] += getValue(data[(x+m)%(size-1)][y]);
			data[x][y] += getValue(data[x][(y-m+(size-1))%(size-1)]);
			data[x][y] += getValue(data[x][(y+m)%(size-1)]);
			data[x][y] /= 4.0f;
			data[x][y] = getSeeds(x, y, data[x][y], m);
		}
		
		setMinMax(data[x][y]);
		
		//for wrap
		if( x==0 ) data[size-1][y] = data[x][y];
		if( y==0 ) data[x][size-1] = data[x][y];
		
		return data[x][y];
	}

	protected void square(int x, int y, int m) {
		if( Float.isNaN( data[x][y] ) ) {
			data[x][y] = (getValue(data[x-m][y-m]) + getValue(data[x-m][y+m]) +
					getValue(data[x+m][y-m]) + getValue(data[x+m][y+m]))/4;
			
			data[x][y] =  getSeeds(x, y, data[x][y], m);
		}

		setMinMax(data[x][y]);
	}
	
	private float getValue(float data) {
		return Float.isNaN(data) ? 0 : data;
	}
	
	private void setMinMax( float val ) {
		max = Math.max(max, val);
		min = Math.min(min, val);
	}

	public static void writeImage(String fileName, float [][] data) {
		BufferedImage bi = new BufferedImage(data.length, data[0].length, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = bi.createGraphics();
		
		for( int x=0; x<data.length; x++ ) {
			for( int y=0; y<data[x].length; y++ ) {
				float col = data[x][y];
				
				try {
					g.setColor(new Color(col, col, col));
				} catch( Exception ex) {
					System.out.println("C: " + col + " [" + x + ", " + y + "]");
					System.exit(1);
				}
				
				g.drawRect(x, y, 1, 1);
			}
		}
		
		File f = new File(fileName);
		try {
			ImageIO.write(bi, "PNG", f);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public static void main(String [] args) {
		DiamondSquare ds3 = new DiamondSquare(9);
		
		//min is (0.0*2*size)-size
		ds3.setMin(-ds3.getSize());
		
		//max is (1.0*2*size)-size
		ds3.setMax(ds3.getSize());
		
		ds3.setWrapped(false);
		float [][] data1 = ds3.generate();
		writeImage("map1.png", data1);

		float [] top = new float[ds3.size];
		for( int x = 0; x<top.length; x++ ) {
			top[x] = data1[x][data1[0].length-1];
		}

		ds3.setSeeds(SEED_TOP, top);
		
		float[][] data2 = ds3.generate();
		writeImage("map2.png", data2);

		float [] left = new float[ds3.size];
		for( int y = 0; y<top.length; y++ ) {
			left[y] = data1[data1.length-1][y];
		}

		ds3.setSeeds(SEED_TOP, null);
		ds3.setSeeds(SEED_LEFT, left);

		float[][] data3 = ds3.generate();
		writeImage("map3.png", data3);
		
		top = new float[ds3.size];
		for( int x = 0; x<top.length; x++ ) {
			top[x] = data3[x][data3[0].length-1];
		}
		left = new float[ds3.size];
		for( int y = 0; y<top.length; y++ ) {
			left[y] = data2[data2.length-1][y];
		}
		ds3.setSeeds(SEED_LEFT, left);
		ds3.setSeeds(SEED_TOP, top);
		
		float[][] data4 = ds3.generate();
		writeImage("map4.png", data4);
		

	}

	public int getSeed() {
		return this.randomSeed;
	}
}
