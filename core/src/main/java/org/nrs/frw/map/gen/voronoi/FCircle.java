package org.nrs.frw.map.gen.voronoi;

public class FCircle extends FPoint {

	public static int circlesCreated = 0;
	
	private boolean deleted = false;
	private double radius;
	
	//this is the curve/site associated with this circle
	private FCurve curve;
		
	private FPoint center;
	
	/**
	 * circles are sorted in the priority queue by their highest
	 * y value, so we add the radius to the center.y value to create
	 * the comparison point. If you want to know where the center
	 * of the circle is use getCenter();
	 * 
	 * @param x
	 * @param y
	 * @param radius
	 */
	public FCircle(double x, double y, double radius) {
		super(x, y+radius);
		center = new FPoint(x, y);
		this.radius = radius;
		circlesCreated++;
	}

	public void setCircle(double x, double y, double radius) {
		setXY(x, y+radius);
		center = new FPoint(x, y);
		this.radius = radius;
		deleted = false;
	}
	
	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public FPoint getCenter() {
		return center;
	}
	
	public void setCenter(double x, double y) {
		center.setXY(x, y);
	}
	
	public void setDeleted(boolean delete) {
		deleted = delete;
	}
	
	public boolean isDeleted() {
		return deleted;
	}

	public void setCurve(FCurve curve) {
		this.curve = curve;
		curve.setCircle(this);
	}
	
	public FCurve getCurve() {
		return curve;
	}


}
