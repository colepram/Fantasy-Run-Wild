package org.nrs.frw.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MapChunk extends Actor {
		SessionManagerInterface tm;
		int [][] tiles;
		
		BitmapFont font;
		Texture gridTexture;
		
		String name;
		
		public MapChunk(String name, SessionManagerInterface tm, int [][] tiles) {
			this.name = name;
			this.tm = tm;
			this.tiles = tiles;
			this.font = new BitmapFont();
						
			setWidth(this.tm.getTileSize()*tiles[0].length);
			setHeight(this.tm.getTileSize()*tiles.length);
			
			Pixmap gpixmap = new Pixmap(this.tm.getTileSize(), this.tm.getTileSize(), Format.RGBA8888);
			gpixmap.setColor(Color.GRAY);
			gpixmap.drawRectangle(0, 0, this.tm.getTileSize(), this.tm.getTileSize());

			gridTexture = new Texture(gpixmap);
			
		}

		@Override
		public void act(float delta) {
			super.act(delta);
			
			Gdx.app.debug("MapChunk.act", "The chunk is acting");
		}
		
		@Override
		public void draw(Batch batch, float parentAlpha) {
			this.draw(batch, parentAlpha, false);
		}
		
		public int getTileX(float x) {
			return (int) (x-getX()-getOriginX())/tm.getTileSize();
		}

		public int getTileY(float y) {
			return (int) (y-getY()-getOriginY())/tm.getTileSize();
		}

		public void draw(Batch batch, float parentAlpha, boolean showGrid) {
			super.draw(batch, parentAlpha);
			if(this.tiles == null ) {
				return;
			}
			
			float xCorrect = getX() + getOriginX() - tm.getTileSize()/2;
			float yCorrect = getY() + getOriginY();
			
			Matrix4 transfor = batch.getTransformMatrix();
			transfor.translate(xCorrect, yCorrect, 0);
			batch.setTransformMatrix(transfor);
			
			for( int y=0; y<this.tiles.length; y++) {
				int [] row = this.tiles[y];
				for( int x=0;  x<row.length; x++ ) {
					float xOff = x*tm.getTileSize();
					float yOff = y*tm.getTileSize();
	
					batch.draw(this.tm.getTile(row[x]), xOff, yOff);
					if( showGrid ) {
						batch.draw(gridTexture, xOff, yOff);
					}
				}
			}
			
			transfor.translate(-xCorrect, -yCorrect, 0);
			batch.setTransformMatrix(transfor);
		}
		
		@Override
		public void drawDebug(ShapeRenderer shapes) {

			float xCorrect = getOriginX(); //getX() + getOriginX() - tm.getTileSize()/2;
			float yCorrect = getOriginY();//getY() + getOriginY();
			
			Matrix4 transform = shapes.getTransformMatrix();
			transform.translate(xCorrect, yCorrect, 0);
			shapes.setTransformMatrix(transform);

			shapes.set(ShapeType.Line);
			shapes.setColor(Color.RED);
			shapes.rect(getX(), getY(), 0, 0, getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
			shapes.rect(getX(), getY(), 1, 1, getWidth()-1, getHeight()-1, getScaleX(), getScaleY(), getRotation());

			transform.translate(-xCorrect, -yCorrect, 0);
			shapes.setTransformMatrix(transform);
		}
	}