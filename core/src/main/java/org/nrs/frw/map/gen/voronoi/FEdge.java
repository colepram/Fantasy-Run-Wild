package org.nrs.frw.map.gen.voronoi;

import javax.print.attribute.standard.MediaSize.Other;

public class FEdge {

	public static int edgesCreated = 0;
	
	private FPoint start;
	private FPoint end;
	
	private FPoint left;
	private FPoint right;
	
	private FEdge neighbour;

	private FPoint bisect;
	
	public FEdge(FPoint start, FPoint leftSite, FPoint rightSite) {
		this.start = start;
		this.left = leftSite;
		this.right = rightSite;
		
		edgesCreated++;
	}

	public FEdge(FPoint leftSite, FPoint rightSite) {
		this.left = leftSite;
		this.right = rightSite;
		
		double x = (left.getX() + right.getX())/2;
		double y = (left.getY() + right.getY())/2;
		start = new FPoint(x, y);

		edgesCreated++;
	}

	public FPoint getStart() {
		return start;
	}

	public void setEnd(FPoint point) {
		this.end = point;
	}
	
	public FPoint getEnd() {
		return end;
	}
	
	public FPoint getLeftSite() {
		return left;
	}

	public FPoint getRightSite() {
		return right;
	}

	public void setNeighbour(FEdge neighbour) {
		this.neighbour = neighbour;
		neighbour.neighbour = this;
	}

	public FEdge getNeighbour() {
		return neighbour;
	}

	public FPoint bisect(FEdge otherEdge) {
		
		//y = m * x + b sound familiar?
		
		//use the negative reciprocal of the slope for bisecting lines
		double m1 = -1/getSlope();
		double m2 = -1/otherEdge.getSlope();
		
		//have to calculate new y intercepts for bisecting lines
		double b1 = start.getY() - start.getX()*m1;
		double b2 = otherEdge.getStart().getY() - otherEdge.getStart().getX()*m2;
		
		double x = 0;
		double y = 0;
		//if the slope of m1 or m2 is infinite it's because it's a vertical line.
		if( Double.isInfinite(m1) ) {
			x = start.getX();
			y =  Double.isInfinite(m2) ? start.getY() : m2*x+b2;
		} else if( Double.isInfinite(m2) ){
			x = otherEdge.getStart().getX();
			y = Double.isInfinite(m1) ? start.getY() : m1*x+b1;
		} else if(m1==0 || m1==-0) {
			x = start.getX();
			y = (start.getY()+otherEdge.getStart().getY())/2;
		}else {
			x = (b1-b2) / (m2-m1);
			y = m1*x+b1;
		}
		
		if( Double.isNaN(x) || Double.isNaN(y)) {
			System.out.println("NAN [" + x + ", " + y + "] " + start + ":" + otherEdge.getStart() + " equal: " + start.equals(otherEdge.getStart()));
			return null;
		}

		if( bisect == null ) {
			bisect = new FPoint(x, y);
		}

		bisect.setXY(x, y);
		return bisect;
	}

	public double getIntercept() {
		return start.getY()-start.getX()*getSlope();
	}

	public double getSlope() {
		return (right.getY() - left.getY()) / (right.getX()-left.getX());
	}
	
	public void closeEdge(double width, double height) {
		if( end != null ) {
			return;
		}
		
		end = new FPoint(start.getX(), start.getY());
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Start ");
		sb.append(start!=null?start.toString():"[null]");
		sb.append(", End ");
		sb.append(end!=null?end.toString():"[null]");
		sb.append(" Left ");
		sb.append(left!=null?left.toString():"[null]");
		sb.append(" Right ");
		sb.append(right!=null?right.toString():"[null]");
		
		return sb.toString();
	}

	public void setBisect(FPoint bisect) {
		this.bisect = bisect;
	}
}
