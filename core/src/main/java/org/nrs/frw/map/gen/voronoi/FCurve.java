package org.nrs.frw.map.gen.voronoi;

/**
 * FCurve is more of a concept than a physical curve. The class is used to
 * construct a binary search tree that mimics the way that curves would over
 * lap each other. That said the "site" of the curve is used as a parabolic
 * curves focal point and used to compute a beach line in the Voronoi -
 * Fortune algorithm.
 * 
 * @author colepram
 *
 */
public class FCurve {

	private FCurve parent;
	
	private FCurve left;
	private FCurve right;

	private FPoint site;
	private FEdge edge;
	private FCircle circle;
	
	//Memory management:
	//instead of creating a new bisection point
	//whenever it needs to be checked this allows
	//us to just update an existing point.
	private FPoint bisect;

	public FCurve() {
	}

	public FCurve(FPoint site) {
		this.site = site;
	}
	
	public FPoint getSite() {
		return site;
	}
	
	public void setEdge(FEdge edge) {
		this.edge = edge;
	}

	public FEdge getEdge() {
		return edge;
	}

	public void setParent(FCurve parent) {
		this.parent = parent;
	}
	
	public FCurve getParent() {
		return parent;
	}

	public void setLeft(FCurve leftChild) {
		left = leftChild;
		left.setParent(this);
	}

	public FCurve getLeft() {
		return left;
	}

	public void setRight(FCurve rightChild) {
		right = rightChild;
		right.setParent(this);
	}
	
	public FCurve getRight() {
		return right;
	}

	public boolean isLeaf() {
		return left == null && right == null;
	}

	public FCurve getRightParent() {
		FCurve n = this;
		FCurve p = n.parent;
		
		while(p!=null && p.right == n) {
			n = p;
			p = p.parent;
		}
		return p;
	}

	public FCurve getLeftParent() {
		FCurve n = this;
		FCurve p = n.parent;
		
		while(p!=null && p.left == n) {
			n = p;
			p = p.parent;
		}
		return p;
	}

	public FCurve getSmallestRight() {
		FCurve n = right;
		while(n!=null && !n.isLeaf()) {
			n = n.left;
		}
		return n;
	}
	
	public FCurve getLargestLeft() {
		FCurve n = left;
		while(n!=null && !n.isLeaf()) {
			n = n.right;
		}
		return n;
	}
	
	public void remove() {
		if( parent == null || parent.parent == null ) {
			return;
		}
		
		FCurve grandParent = parent.parent;
		if( grandParent.left == parent ) {
			grandParent.setLeft( parent.left == this ? parent.right : parent.left);
		} else {
			grandParent.setRight(parent.right == this ? parent.left : parent.right);
		}
	}

	public FCurve findSite(double x, double sweep) {
		
		FCurve node = this;
		while(node!=null && !node.isLeaf()) {
			double xx = this.getBreakPointX(node, sweep);
			
			if( xx < x ) {
				node = node.getRight();
			} else {
				node = node.getLeft();
			}
		}
		
		return node;
	}

	/**
	 * getBreakPoint this is where the Ma(th)gic happens
	 * I spent a week re-learning everything about parabolic curves
	 * and the quadratic formula to make this work.
	 * 
	 * @param node
	 * @return
	 */
	private double getBreakPointX(FCurve node, double sweep) {

		FCurve left = node.getLargestLeft();
		FCurve right = node.getSmallestRight();
		
		if( left.getSite().equals(right.getSite()) ) {
			return left.getSite().getX();
		}
		
		double lp = left.site.getY() - sweep; // same as y-k
		double rp = right.site.getY() - sweep; // same as y-k
		
		//if one of the points is on the sweepline just return its x
		if( lp == 0 && rp == 0 ) {
			if( left.getSite().getX() > node.getSite().getX() ) {
				return left.getSite().getX();
			}
			return right.site.getX();
		}
		
		//MATH!!!!
		//intersection of 2 parabolic curves 4(y-k) = (x-h)^2 solve for x where y1==y2
		double h1 = left.site.getX();
		double h2 = right.site.getX();

		double a = 1/(4*lp) - 1/(4*rp);
		double b = h2/(2*rp) - h1/(2*lp);
		double c = (lp/4 + (h1*h1)/(4*lp)) - (rp/4 + (h2*h2)/(4*rp));

		if( a == 0 ) {
			return -c/b;
		}

		double sqrt = Math.sqrt(b*b - 4*a*c);
		
		double x1 = (-b - sqrt)/(2*a);
		double x2 = (-b + sqrt)/(2*a);
		
		//now do we want the left intersection or the right?
		return left.getSite().getY() < right.getSite().getY() ? Math.max(x1, x2) : Math.min(x1, x2);
	}
	
	public FPoint getBisectingPoint(double x, double y) {
		double nx, ny;
		
		if( bisect == null ) {
			bisect = new FPoint(x, y);
		}

		if( y == site.getY() ) {
			nx = (x+site.getX())/2;
			ny = y;
		} else {
			double m = -(x - site.getX()) / (y - site.getY());
			double midX = (x+site.getX())/2;
			double midY = (y+site.getY())/2;
			
			double b = midY - m*midX;
			
			nx = x;
			ny = m*x + b;

			if( ny < 0 ) {
				ny = 0;
				nx = (ny-b) / m;
			}
		}

		bisect.setXY(nx, ny);
		
		return bisect;
	}
	
	@Override
	public boolean equals(Object obj) {
		if( obj instanceof FCurve && (this.site != null && ((FCurve)obj).getSite()!=null)) {
			return this.site.equals(((FCurve)obj).getSite());
		}
		return super.equals(obj);
	}
	
	public String toString() {
		
		return site!=null ? site.toString() : super.toString();
	}

	public FCircle getCircle() {
		return circle;
	}

	public void setCircle(FCircle circle) {
		this.circle = circle;
	}
}
