package org.nrs.frw.debug;

import java.util.ArrayList;

import org.nrs.FantasyRunWild;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class DebugConsole extends Actor {

	float opacity = 0.75f;
	FantasyRunWild game;

	Texture background;

	BitmapFont font;

	GlyphLayout layout = new GlyphLayout();
	
	ArrayList<String> lines = new ArrayList<String>();

	public DebugConsole(FantasyRunWild game) {
		this.game = game;
		
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		font.getData().setScale(0.5f);
		
		Pixmap pm = new Pixmap(1, 1, Format.RGBA8888);
		pm.setColor(new Color(Color.GRAY.r, Color.GRAY.g, Color.GRAY.b, opacity));
		pm.fillRectangle(0, 0, 1, 1);
		background = new Texture(pm);
		
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		Matrix4 translate = batch.getTransformMatrix();
		translate.translate(getOriginX(), getOriginY()-getHeight(), 0);
		batch.setTransformMatrix(translate);

		batch.draw(background, 0, 0, getWidth(), getHeight());

		font.draw(batch, layout, 0, getHeight()-1);
		
		translate.translate(-getOriginX(), -getOriginY()+getHeight(), 0);
		batch.setTransformMatrix(translate);
	}

	public void setText(String text) {
		layout.setText(font, text);
		
		setHeight(layout.height + 2);
		setWidth(layout.width + 2);
	}
}
