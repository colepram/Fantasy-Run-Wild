package org.nrs.frw.character;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasSprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class SpriteActor extends Actor {

	private HashMap<String, NamedAnimation<Sprite>> animationMap;
	private HashMap<String, Sprite> spriteMap;
	
	private ArrayList<String> spriteLayers;
	private ArrayList<NamedAnimation<Sprite>> animationQueue;
		
	public SpriteActor() {
		spriteLayers = new ArrayList<String>();
		animationMap = new HashMap<String, NamedAnimation<Sprite>>();
		spriteMap = new HashMap<String, Sprite>();
		
		animationQueue = new ArrayList<NamedAnimation<Sprite>>();
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		setPosition(getX(), getY());
		setSize(getWidth(), getHeight());
		for( String lName : spriteLayers ) {
			spriteMap.get(lName).draw(batch);
		}
	}
	
	public void animate(String layer) {
		animate(layer, null);
	}

	public void animate(String layerName, PlayMode playMode) {
		if( animationMap.containsKey(layerName) ) {
			NamedAnimation<Sprite> a = animationMap.get(layerName);
			if( !animationQueue.contains(a) ) {
				a.setPlayMode((playMode==null)?a.getPlayMode():playMode);
				animationQueue.add(a);
			}
		}
	}

	@Override
	public void act(float delta) {
		if( !animationQueue.isEmpty() ) {
			for( int i = animationQueue.size()-1; i>=0; i--) {
				NamedAnimation<Sprite> a = animationQueue.get(i);
				String lName = animationQueue.get(i).getPlayLayer();
				
				spriteMap.get(lName).set(a.getKeyFrame(delta));
				if( a.isAnimationFinished(delta) ) {
					animationQueue.remove(i);
				}
			}
		}
	}
	
	public void setLayerColor(String layerName, Color color) {

		int index = spriteLayers.indexOf(layerName);
		if( index < 0 ) {
			return;
		}
		
		spriteMap.get(layerName).setColor(color);
		for( String animeName : animationMap.keySet()) {
			NamedAnimation<Sprite> anime = animationMap.get(animeName);
			if( anime != null && anime.getPlayLayer().equalsIgnoreCase(layerName) ) {
				for( Sprite s : anime.getKeyFrames() ) {
					s.setColor(color);
				}
			}
		}
	}
	
	@Override
	public void setScale(float x, float y) {
		super.setScale(x, y);
		
		for( String layerName : spriteLayers ) {
			spriteMap.get(layerName).setScale(x, y);
		}
	}
	
	public AtlasSprite getLayer(String layer) {
		try {
			return (AtlasSprite)spriteMap.get(layer);
		} catch (IndexOutOfBoundsException ex ) {
			return null;
		}
	}

	public AtlasSprite getLayer(int layer) {
		try {
			return (AtlasSprite)spriteMap.get(spriteLayers.get(layer));
		} catch (IndexOutOfBoundsException ex ) {
			return null;
		}
	}
	
	public void addLayer(String layerName, Sprite s) {
		
		if( s == null ) {
			return; //There is not sprite in the atlas with this layer name
		}
		spriteMap.put(layerName, s);

		if( !spriteLayers.contains(layerName) ) {
			spriteLayers.add(layerName);
		}

		if( s instanceof AtlasSprite ) {
			AtlasRegion ar = (AtlasRegion)((AtlasSprite)s).getAtlasRegion();
			int height = ar.originalHeight;
			int width = ar.originalWidth;
			setSize(width, height);
		}
	}
	
	@Override
	public void setPosition(float x, float y) {
		super.setPosition(x, y);

		for( String layerName : spriteLayers ) {
			spriteMap.get(layerName).setPosition(x, y);
		}
	}
	
	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		
		for( String layerName : spriteLayers ) {
			spriteMap.get(layerName).setSize(width, height);;
		}		
	}
	
	public void addAnimation(String layerName, String playOnLayer, Array<Sprite> spriteArray) {
		//Array<Sprite> spriteArray = atlas.createSprites(layerName);

		if( spriteArray.size <= 0 ) {
			return; //no sprites to load.
		}
		NamedAnimation<Sprite> animation = new NamedAnimation<Sprite>(playOnLayer, 0.3f, spriteArray, PlayMode.NORMAL);
		animationMap.put(layerName, animation);
	}

	private class NamedAnimation<T> extends Animation<T> {
		
		private String playOnLayer;
		
		public NamedAnimation(String playOnLayer, float frameDuration, Array<? extends T> keyFrames, PlayMode playMode) {
			super(frameDuration, keyFrames, playMode);
			this.playOnLayer = playOnLayer;
		}

		public String getPlayLayer() {
			return playOnLayer;
		}
	}
}
