package org.nrs.frw.character;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;
import org.nrs.frw.character.Player.SpriteContainer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class Player extends Actor {

	TreeMap<Integer,SpriteContainer> spriteTree = new TreeMap<>();
	
	HashMap<String, Color> colorMap = new HashMap<>();
	ArrayList<String> layers = new ArrayList<>();
	
	FantasyRunWild game;
	
	float playerWidth = -1;
	float playerHeight = -1;
	
	public Player(FantasyRunWild game) {
		this.game = game;
		
		initDefaults();

		TextureAtlas playerAtlas = game.manager.get(GameConstants.ATLAS_BLANKS, TextureAtlas.class);
		for( String layer : layers ) {
			AtlasRegion ar = playerAtlas.findRegion(layer);
			Sprite s = playerAtlas.createSprite(layer);
			SpriteContainer sc = new SpriteContainer(ar, s);
			
			spriteTree.put(sc.layerIndex, sc);
			
			if( playerWidth == -1 ) {
				playerWidth = ar.originalWidth;
			}

			if( playerHeight == -1 ) {
				playerHeight = ar.originalHeight;
			}
		}
		
		setOrigin(0, 0);
		setSize(playerWidth, playerHeight);
	}
	
	public abstract void initDefaults();
	
	public List<String> getLayers() {
		return layers;
	}

	@Override
	public void act(float delta) {
		super.act(delta);
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		//Color curColor = batch.getColor();

		float xOffset = getX();// tmi.getTileSize()/2;
		float yOffset = getY();//tmi.getTileSize()/3;
		
		Matrix4 scale = batch.getTransformMatrix();
		scale.translate(xOffset, yOffset, 0);
		scale.scale(getScaleX(), getScaleY(), 1);
		batch.setTransformMatrix(scale);

		for( SpriteContainer sc : spriteTree.values() ) {
			batch.setColor(sc.sprite.getColor());
			sc.sprite.draw(batch, parentAlpha);
		}
		
		//batch.setColor(curColor);
		
		scale.translate(-xOffset, -yOffset, 0);
		scale.scale(1/getScaleX(), 1/getScaleY(), 1);
		batch.setTransformMatrix(scale);
	}

	@Override
	public void drawDebug(ShapeRenderer shapes) {

		Gdx.app.log("Shape Type", shapes.getCurrentType().name());
		Gdx.app.log("[x, y]", "[" + getX() + ", " + getY() + "]");
		Gdx.app.log("[ox, oy]", "[" + getOriginX() + ", " + getOriginY() + "]");
		Gdx.app.log("[w, h]", "[" + getWidth() + ", " + getHeight() + "]");
		Gdx.app.log("[sx, sy]", "[" + getScaleX() + ", " + getScaleY() + "]");
		shapes.rect(getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), 0, Color.RED,  Color.RED,  Color.RED,  Color.RED);

		//super.drawDebug(shapes);
		
	}

	protected HashMap<String, Color> getColorMap() {
		return colorMap;
	}

	class SpriteContainer {
		AtlasRegion ar;
		Sprite sprite;

		Integer facing;
		String colorGroup;
		Integer layerIndex;
		Integer style;
		
		public SpriteContainer(AtlasRegion region, Sprite sprite) {
			this.ar = region;
			this.sprite = sprite;
			
			String [] vals = ar.name.split("_");
			facing =  new Integer(vals[0].substring(1, vals[0].length()));
			colorGroup = (layerIndex = new Integer(vals[1].substring(1, vals[1].length()))).toString();
			style =  new Integer(vals[2].substring(1, vals[2].length()));
			
			if( ar.name.contains("_C") ) {
				colorGroup = vals[3];
			}

			this.sprite.setColor(colorMap.getOrDefault(colorGroup.toString(), Color.WHITE));
		}
	}
}
