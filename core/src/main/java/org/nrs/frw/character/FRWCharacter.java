package org.nrs.frw.character;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Json.Serializable;
import com.badlogic.gdx.utils.JsonValue;

public class FRWCharacter implements Serializable {
	
	public static final int FRONT = 0;
	public static final int BACK = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;
	
	public static final String FIELD_NAME = "name";
	public static final String FIELD_ATLAS = "atlas";
	public static final String FIELD_LAYERS = "layers";

	public enum BASE_LAYER {
		BODY(0, "body"),
		CLOTHS(1, "cloths"),
		HAIR(2, "hair"),
		EYES(3, "eyes");
		
		public int index;
		public String label;
		private BASE_LAYER(int index, String label) {
			this.index = index;
			this.label = label;
		}
		
		public String toString() {
			return label;
		}
	}

	private HashMap<String,LayerContainer> layerArray = new HashMap<String,LayerContainer>();
	private ArrayList<String> atlasArray = new ArrayList<String>();
	
	private FantasyRunWild game;
	private SpriteActor [] facings = new SpriteActor[4];

	//The uuid is how this character will be identified to the game
	private UUID characterIdentifier;

	//The atlas to use when acquiring assets for this character. The Textures
	//are held by the game's asset manager
	private String characterName;
	
	public FRWCharacter(FantasyRunWild game) {
		this();
		this.game = game;
	}
	
	public FRWCharacter(FantasyRunWild game, UUID uuid) throws IOException {
		this(game);
		
		loadCharacter(uuid);
	}
	
	public FRWCharacter() {
		facings = new SpriteActor[4];
	}
	
	public UUID saveCharacter() throws FileNotFoundException, IOException {
		
		if(characterIdentifier == null ) {
			characterIdentifier = UUID.randomUUID();
		}
		Json json = new Json();

		//create folder based on UUID
		FileHandle out = Gdx.files.local(GameConstants.SAVE_DIR + characterIdentifier + "/character.dat");

		if( out.exists() ) {
			out.delete();
		}
		out.writeString(json.toJson(this), false);
		//out.write(false).write(json.toJson(this).getBytes());
		return characterIdentifier;
	}
	
	public void loadCharacter(UUID uuid) throws IOException {
		//this.game = game;
//		characterIdentifier = UUID.fromString("9e060e7a-600a-4ffa-8e5f-dd1a1d71c0ec");
		characterIdentifier = uuid;
		
		FileHandle in = Gdx.files.local(GameConstants.SAVE_DIR + characterIdentifier + "/character.dat");
		
		Json json = new Json();

		FRWCharacter tmp = json.fromJson(FRWCharacter.class, in.read());
		
		setName(tmp.characterName);
		
		for( LayerContainer l : tmp.layerArray.values() ) {
			if( l.animationName != null ) {
				addAnimation(l.face, tmp.atlasArray.get(l.atlasName), l.textureLayer, l.animationName, l.layerName);
			} else {
				addLayer(l.face, tmp.atlasArray.get(l.atlasName), l.textureLayer, l.layerName);
			}
			
			if( l.layerColor != null ) {
				setLayerColor(l.layerName, l.layerColor);
			}
		}
	}

	public SpriteActor getFacing(int facing) {
		if( facings[facing] == null ) {
			facings[facing] = new SpriteActor();
		}
		return facings[facing];
	}
	
	public void setFacing(int facing, SpriteActor actor) {
		facings[facing] = actor;
	}

	public void setLayerColor(String layer, Color color) {
		for( SpriteActor s : facings ) {
			if( s != null ) {
				s.setLayerColor(layer, color);
			}
		}
		
		for( LayerContainer l : layerArray.values() ) {
			if( l.layerName.contains(layer) ) {
				l.layerColor = color;
			}
		}
	}

	public String getName() {
		return characterName;
	}
	
	public void setName(String name) {
		this.characterName = name;
		Gdx.app.debug("FRWCharacter: setname", "Name Changed to: " + name);
	}

	public String getTextureLayerFor(int face, String layerName) {
		String key = face + "_" + layerName;
		
		return layerArray.get(key).textureLayer;
	}
	
	public void addLayer(int face, String atlasName, String textureLayer, String layerName) {
		TextureAtlas ta = game.manager.get(atlasName, TextureAtlas.class);
		getFacing(face).addLayer(layerName, ta.createSprite(textureLayer));
		
		String key = face+"_"+layerName;
		LayerContainer cont = layerArray.get(key);
		if( cont == null ) {
			cont = new LayerContainer();
		}
		
		cont.face = face;
		cont.atlasName = getAtlas(atlasName);
		cont.textureLayer = textureLayer;
		cont.layerName = layerName;

		layerArray.put(key, cont);
	}

	public void addAnimation(int face, String atlasName, String textureLayer, String animationName, String playOnLayer) {
		TextureAtlas ta = game.manager.get(atlasName, TextureAtlas.class);
		getFacing(face).addAnimation(animationName, playOnLayer, ta.createSprites(textureLayer));

		String key = face+"_"+animationName;
		LayerContainer cont = layerArray.get(key);
		if( cont == null ) {
			cont = new LayerContainer();
		}
		cont.face = face;
		cont.atlasName = getAtlas(atlasName);
		cont.textureLayer = textureLayer;
		cont.layerName = playOnLayer;
		cont.animationName = animationName;		
		layerArray.put(key, cont);
	}

	private int getAtlas(String dir) {
		if( !atlasArray.contains(dir) ) {
			atlasArray.add(dir);
		}
		
		return atlasArray.indexOf(dir);
	}
	
	@Override
	public void write(Json json) {
		json.writeValue(FIELD_NAME, characterName);
		
		json.writeArrayStart(FIELD_ATLAS);
		for( String at : atlasArray ) {
			json.writeValue(at);
		}
		json.writeArrayEnd();
		
		json.writeArrayStart(FIELD_LAYERS);
		for( LayerContainer l : layerArray.values() ) {
			json.writeValue(l);
		}
		json.writeArrayEnd();
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		characterName = json.readValue(FIELD_NAME, String.class, jsonData);
		String[] test = json.readValue(FIELD_ATLAS, String[].class, jsonData);
		for( String s : test) {
			atlasArray.add(s);
		}

		LayerContainer [] containers = json.readValue(FIELD_LAYERS, LayerContainer[].class, jsonData);
		for( LayerContainer c : containers ) {
			String key = c.animationName;
			if( key == null ) {
				key = c.layerName;
			}
			layerArray.put(c.face + "_" +key, c);
		}
	}
	
	static public class LayerContainer implements Serializable {

		public static final String FIELD_NAME = "face";
		public static final String FIELD_ATLAS = "atlas";
		public static final String FIELD_TEXTURE = "texture";
		public static final String FIELD_LAYER = "layerName";
		public static final String FIELD_COLOR = "color";
		public static final String FIELD_ANIMATION = "animation";

		public int face;
		public int atlasName;
		public Color layerColor;
		public String textureLayer;
		public String layerName;
		public String animationName;

		@Override
		public void write(Json json) {
			json.writeValue(FIELD_NAME, face);
			json.writeValue(FIELD_ATLAS, atlasName);
			json.writeValue(FIELD_TEXTURE, textureLayer);
			json.writeValue(FIELD_LAYER, layerName);

			if( layerColor != null ) {
				json.writeValue(FIELD_COLOR, layerColor);
			}
			if( animationName != null ) {
				json.writeValue(FIELD_ANIMATION, animationName);
			}
		}
		@Override
		public void read(Json json, JsonValue jsonData) {
			face = json.readValue(FIELD_NAME, Integer.class, jsonData);
			atlasName = json.readValue(FIELD_ATLAS, Integer.class, jsonData);
			textureLayer = json.readValue(FIELD_TEXTURE, String.class, jsonData);
			layerName = json.readValue(FIELD_LAYER, String.class, jsonData);
			layerColor = json.readValue(FIELD_COLOR, Color.class, jsonData);
			animationName = json.readValue(FIELD_ANIMATION, String.class, jsonData);
		}

	}

}
