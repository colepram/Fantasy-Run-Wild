package org.nrs.frw.character;

public interface TurnListenerInterface {

	public void takeTurn();
}
