package org.nrs.frw.character;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

import org.nrs.FantasyRunWild;
import org.nrs.frw.ControlConstants;
import org.nrs.frw.GameConstants;
import org.nrs.frw.map.SessionManagerInterface;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.Actor;

//this is the basics of a player
public class TestPlayer extends Player {

	private static final float COOL_DOWN_SPEED = 0.25f;

	boolean debug = true;
	
	private float WALK_SPEED = 1;
	private float RUN_SPEED = 2;
	
	float movementSpeed = WALK_SPEED;
	
	float turnCoolDown = 0;
	float turnCoolOff = 3;

	ArrayList<TurnListenerInterface> turnListeners = new ArrayList<TurnListenerInterface>();
	
	SessionManagerInterface sessionManager;
	
	public TestPlayer(FantasyRunWild game, SessionManagerInterface sessionManager) {
		super(game);
		
		this.sessionManager = sessionManager;

		setScale((float)sessionManager.getTileSize()/playerWidth, (float)sessionManager.getTileSize()/playerHeight);
	}
	
	public void initDefaults() {
		colorMap.put("hair", Color.YELLOW);
		colorMap.put("eyes", Color.BLUE);
		colorMap.put("body", Color.SALMON);
		colorMap.put("head", Color.SALMON);
		colorMap.put("cloths", Color.RED);

		layers.add("10_front_hair_s2");
		layers.add("20_front_body_s1");
		layers.add("30_front_cloths_s1");
		layers.add("40_front_head_s1");
		layers.add("50_front_hair_s1");
		layers.add("60_front_eyes_s1");
	}
	
	@Override
	public void act(float delta) {
		super.act(delta);

		if( turnCoolDown > 0 ) {
			turnCoolDown -= COOL_DOWN_SPEED;
			return;
		}
		
		movementSpeed = WALK_SPEED*sessionManager.getTileSize();
		if( ControlConstants.RUN.isPressed() ) {
			movementSpeed = RUN_SPEED*sessionManager.getTileSize();
		}
		
		if( ControlConstants.RIGHT.isPressed() ) {
			moveBy(movementSpeed, 0);
			takeTurn();
		}

		if( ControlConstants.LEFT.isPressed()) {
			moveBy(-movementSpeed, 0);
			takeTurn();
		}

		if(ControlConstants.UP.isPressed() ) {
			moveBy(0, movementSpeed);
			takeTurn();
		}

		if( ControlConstants.DOWN.isPressed()) {
			moveBy(0, -movementSpeed);
			takeTurn();
		}
		
	}
	
	public void takeTurn() {
		turnCoolDown = turnCoolOff;
		for( TurnListenerInterface listener : turnListeners ) {
			listener.takeTurn();
		}
	}

	public void addTurnListener(TurnListenerInterface listener) {
		turnListeners.add(listener);
	}
	
}
