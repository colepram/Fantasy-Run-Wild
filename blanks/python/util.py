from gimpfu import *

def show_all_layers(lyr, *visible):
    print lyr.name + " " + str(type(lyr))
    layers = []

    visible = bool(TRUE if len(visible) == 0 else visible[0])

    if type(lyr) is not gimp.Image:
		pdb.gimp_item_set_visible(lyr, visible)

    if pdb.gimp_item_is_group(lyr):
		for id in pdb.gimp_item_get_children(lyr)[1]:
			show_all_layers(gimp.Item.from_id(id), visible)

def showActive(img):
	show_all_layers(pdb.gimp_image_get_active_layer(img), TRUE)
	
def hideActive(img):
	show_all_layers(pdb.gimp_image_get_active_layer(img), FALSE)
