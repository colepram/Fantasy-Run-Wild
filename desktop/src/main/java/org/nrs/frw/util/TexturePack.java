package org.nrs.frw.util;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.badlogic.gdx.tools.texturepacker.TexturePacker.Settings;

public class TexturePack {

	
	public static void main(String [] args) {
		Settings settings = new Settings();
		settings.stripWhitespaceX=true;
		settings.stripWhitespaceY=true;
		
		//Use for building UI
		//TexturePacker.process(settings, "local/images/UIStyle", "../core/assets/data/images-assets/uistyle", "uistyle");

		TexturePacker.process(settings, "../blanks/character/F0", "../core/assets/data/images-assets", "blank");
	}

}
