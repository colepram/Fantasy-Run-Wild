package org.nrs.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.nrs.FantasyRunWild;
import org.nrs.frw.GameConstants;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = GameConstants.TITLE;
		config.width = GameConstants.WIDTH * GameConstants.SCALE;
		config.height = GameConstants.HEIGHT * GameConstants.SCALE;
		
		new LwjglApplication(new FantasyRunWild(), config);
	}
}
